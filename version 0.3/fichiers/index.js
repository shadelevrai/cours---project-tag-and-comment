$(function () {
    $('.azerty').click(function () {
        if ($('.notation').css('display') == 'none') {
            $('.notation').css({
                display: '',
            })
        } else {
            $('.notation').css({
                display: 'none',
            })
        }
    })

    //la fonction cursor
    $('.changeStar').css({
        cursor: "pointer"
    })

    //première étoile
    $('.starYellow1').hide();

    $('.stars1').mouseenter(function () {
        $(this).hide();
        $('.starYellow1').show();
    })

    $('.starYellow1').mouseleave(function () {
        $(this).hide();
        $('.stars1').show();
    })

    //second étoile
    $('.starYellow2').hide();

    $('.stars2').mouseenter(function () {
        $('.stars1').hide();
        $(this).hide();
        $('.starYellow1').show();
        $('.starYellow2').show();
    })

    $('.starYellow2').mouseleave(function () {
        $('.starYellow1').hide();
        $(this).hide();
        $('.stars1').show();
        $('.stars2').show();
    })

    //troisième étoile
    $('.starYellow3').hide();

    $('.stars3').mouseenter(function () {
        $('.stars1').hide();
        $('.stars2').hide();
        $(this).hide();
        $('.starYellow1').show();
        $('.starYellow2').show();
        $('.starYellow3').show();
    })

    $('.starYellow3').mouseleave(function () {
        $('.starYellow1').hide();
        $('.starYellow2').hide();
        $('.starYellow3').hide();
        $('.stars1').show();
        $('.stars2').show();
        $('.stars3').show();
    })

    //quatrième étoile
    $('.starYellow4').hide();

    $('.stars4').mouseenter(function () {
        $('.stars1').hide();
        $('.stars2').hide();
        $('.stars3').hide();
        $('.stars4').hide();
        $('.starYellow1').show();
        $('.starYellow2').show();
        $('.starYellow3').show();
        $('.starYellow4').show();
    })

    $('.starYellow4').mouseleave(function () {
        $('.starYellow1').hide();
        $('.starYellow2').hide();
        $('.starYellow3').hide();
        $('.starYellow4').hide();
        $('.stars1').show();
        $('.stars2').show();
        $('.stars3').show();
        $('.stars4').show();
    })

    //cinquième étoile
    $('.starYellow5').hide();

    $('.stars5').mouseenter(function () {
        $('.stars1').hide();
        $('.stars2').hide();
        $('.stars3').hide();
        $('.stars4').hide();
        $('.stars5').hide();
        $('.starYellow1').show();
        $('.starYellow2').show();
        $('.starYellow3').show();
        $('.starYellow4').show();
        $('.starYellow5').show();
    })

    $('.starYellow5').mouseleave(function () {
        $('.starYellow1').hide();
        $('.starYellow2').hide();
        $('.starYellow3').hide();
        $('.starYellow4').hide();
        $('.starYellow5').hide();
        $('.stars1').show();
        $('.stars2').show();
        $('.stars3').show();
        $('.stars4').show();
        $('.stars5').show();
    })

    //------------------------système d'obligation de données-------------------------------//

    var tag = $('#tag'),
        comm = $('#comm'),
        champ = $('.champ');
    //note = $('input[type=radio][name=note]:checked');

    tag.blur(function () {
        if ($(this).val().length < 4) {
            $(this).css({
                borderColor: 'red'
            })
            $('.firstDivError').text('Votre pseudo doit contenir au moins 4 charactères').css("fontSize", "12px");
        } else {
            $(this).css({
                borderColor: 'green'
            })
            $('.firstDivError').text('');
        }
    })

    /*comm.blur(function () {
        if ($(this).val().length < 20) {
            $(this).css({
                borderColor: 'red'
            })
            $('.secondDivError').text('Votre commentaire doit contenir au moins 20 charactères').css("fontSize", "12px");
        } else {
            $(this).css({
                borderColor: 'green'
            })
            $('.secondDivError').text('');
        }
    })*/


    $('.laValidation').click(function (event) {
        if (tag.val().length > 3) {} else {
            event.preventDefault();
        }
    })

    //-------------------------------------étoiles jaunes statiques------------------------------------------//

    $('.starYellow1').click(function () {
        $('.starYellow1').show();
        $('.starYellow2').hide();
        $('.starYellow3').hide();
        $('.starYellow4').hide();
        $('.starYellow5').hide();
        $('.stars1').hide();
        $('.stars2').show();
        $('.stars3').show();
        $('.stars4').show();
        $('.stars5').show();
    })
})