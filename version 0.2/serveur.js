var express = require('express');
var bodyParser = require("body-parser");
var app = express();
//var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017/jeux';
//var fichierJs = require('./fichiers/module');
//var $ = require('jquery');

var myDate = new Date;


app.use(express.static(__dirname + '/fichiers'));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/fichiers'))



//------------------Page d'acceuille-----------------------------//
app.get('/', function (req, res) {
    res.render('index.html');
})

app.get('/api/avis/:avis_id', function (req, res) {
    
})

app.get('/admin', function(req,res){
    res.render('admin');
})
//----------------------Envois d'infos--------------------------//
app.post('/sendInformation', function (req, res) {
    var tag = req.body.tag;
    var commentary = req.body.commentary;
    var note = req.body.note;
    console.log('--------------------');
    console.log('pseudo de l\'utilisateuuuuuuuur : ' + tag);
    console.log('commentaire : ' + commentary);
    console.log('note : ' + note);
    console.log('date : ' + myDate);
    console.log('--------------------');
    //res.render('index');
    res.redirect('http://localhost:8080/');
    
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').insert({
                Pseudo: tag,
                Commentaire: commentary,
                Note: note,
                Date : myDate
            });

            db.collection('temp').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    console.log(result);
                }
            });
            
        }
    });
    
})

//---------------------------------test--------------------------------//


//----------------------------Get et post-------------------------------------//
app.get('/api/affiche', function (req,res){
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);        
                }
            });
        }
    });
})


app.post('/api/affiche2', function (req,res){
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);
            //var lalala = db.collection('temp').findOne();
            db.collection('temp').deleteOne();
            //console.log(lalala);
            console.log('Le commentaire ont été supprimé');
        }
    });
})


var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});
/*
Etape 1 : Création d'une page 'index' où on peut entrer un pseudo, un commentaire et une note sur 5.
Etape 2 : Les infos (pseudo, commentaire, note, date) sont envoyées dans une base de donnée temporaire (mongoDB)
Etape 3 : Création d'une page d'administration qui sera protégée.
Etape 4 : Le serveur envoit et affiche les infos de la base de donnée temporaire vers la page d'administration.
Etape 5 : Les infos peuvent être validées ou refusées. Si elles sont refusées, elles sont supprimées de la collection temporaire de la base de donnée, envoyées vers une collection "refuse" et les infos sont instantanement supprimées de la page d'admin.
Etape 6 : Si les infos sont validées, elles sont supprimées de la collection temporaire, envoyées dans une collection "accepted", supprimées de la page d'admin.
Etape 7 : Les infos de la collection "accepted" sont affichées sur la page "index".
*/