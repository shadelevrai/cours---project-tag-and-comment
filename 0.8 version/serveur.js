//Le modules
var express = require('express'),
    bodyParser = require("body-parser"),
    session = require('express-session'),
    cookieParser = require('cookie-parser'),
    app = express(),
    //mongoose = require('mongoose'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://cernnunos:Zmfgvknrzs11@ds141209.mlab.com:41209/jeux';
var device = require('express-device'),
    morgan = require('morgan'),
    fs = require('fs'),
    path = require('path'),
    log4js = require('log4js'),

    //Module complémentaire pour MORGAN
    accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
        flags: 'a'
    }),

    myDate = new Date;


app.use(cookieParser('SECRET'));
app.use(device.capture()); //Module pour connaitre la plate-forme de connection (PC, tablette, mobile...)
app.use(express.static(__dirname + '/fichiers'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(morgan('combined', {
    stream: accessLogStream
}));
app.use(session({
    secret: 's3Cur3',
    name: 'sessionId',
})); // Système de session. A faire.


app.set('view engine', 'jade'); // Template HTML
app.set('views', './fichiers'); // Dire à JADE où sont les fichiers.


log4js.configure({
    appenders: [
        {
            type: 'console'
        },
        {
            type: 'file',
            filename: 'console.log',
            category: 'Console'
        }
  ]
}); // Système de log



var logger = log4js.getLogger('Console');

//------------------Mot refusés-------------------------------------//

//Base d'insulte pour la modération automatique.
var insulte = /(salope|pute|merde|fuck|abruti|branler|chier|chieur| con.| con | con, | cons.| cons |connar|connard| conne |couillon|crétin|cretin| cul |enculé|encule|enflure|enfoiré|enfoire|foutre|garce|gouine|pd|grognasse|merdeux|niquer| nique |pédé|pétasse|petasse|pede|péteux|peteux|pouffiasse|poufiasse|putain|put1|salaud|salop|salope|saloperie|saloperi|salopard|salopar|traînée|trainée|trainé|fdp|étron|fiote| bite |bougnoul|bougnoule|branleur|couille|couilles|lavette|lopette|négro|nègre|negre|fucker|enculer|salopes|putes|merdes|fucks|abrutis|branlers|chiers|chieurs| cons |connars|connards|connes|couillons|crétins|cretins|culs|enculés|encules|enflures|trouduq|trou du q|enfoirés|enfoires|foutres|garces|gouines|pds|grognasses|merdeux|niquer|niques|pédés|pétasses|petasses|pedes|péteux|peteux|pouffiasses|poufiasses|putain|put1|salauds|salop|salopes|saloperies|saloperi|salopards|salopars|traînées|trainées|trainés|fdp|étrons|fiotes|bites|bougnouls|bougnoules|batard|branleurs|couilles|couilles|lavettes|lopettes|négros|nègres|negres|fucker|enculer|http:|www|{|}|@)/i;
//------------------Page enquete-----------------------------//

/*app.get('/', function(req, res) {
  console.log("Cookies: ", req.cookies);
  //console.log('------');
  console.log(req.session);
    //console.log('-----');
})*/

//Essai pour savoir le device de connection
app.get('/hello', function (req, res) {
    res.send("Hi to " + req.device.type.toUpperCase() + " User");
});

//La page de refus de connection
app.get('/refus', function (req, res) {

    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('refus');
    } else {
        res.render('./Mobile/refusMobile');
    }

});

//La page de refus de dépot d'avis
app.get('/accesdenied', function (req, res) {
    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('accesDenied');
    } else {
        res.render('./Mobile/accesDenied');
    }
});

//LA première page
app.get('/', function (req, res) {

    //var sess = req.session;
    //if (sess.views) {


    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress; //Variable qui sert à noter l'adresse IP de connection dans le fichier log

    //Pour prendre les paramètres présent dans le lien
    name = req.query["name"];
    formationName = req.query["formationname"];
    formationPlace = req.query["formationplace"];
    beginDate = req.query["begindate"];
    endingDate = req.query["endingdate"];

    //Connection à la base de donnée
    MongoClient.connect(url, function (err, db) {

        //Si MongoDB est pas dispo
        if (err) {
            logger.warn('Unable to connect to the mongoDB server. Error:', err);
        } else {

            //Si le nom n'est pas en paramètre
            if (name == null) {
                res.render('refus entree');
            } else {

                //Si la personne a déjà déposé son avis mais tente de le refaire
                db.collection('temp').find({
                    name: name,
                    beginDate: beginDate
                }).count(function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        if (result > 0) {

                            //Si la personne a déposé son avis mais tente de le refaire, message sur la console et fichier log
                            logger.warn(name + ' a tenté de redonné son avis ' + ip);
                            if (req.device.type.toUpperCase() == "DESKTOP") {
                                res.render('accesDenied');
                            } else {
                                res.render('./Mobile/accesDenied');
                            }
                        } else {

                            //Si tout est ok, La page principale est envoyée avec les paramètres mis dans des variables
                            if (req.device.type.toUpperCase() == "DESKTOP") {
                                res.render('index', {
                                    name: name,
                                    formationName: formationName,
                                    formationPlace: formationPlace,
                                    beginDate: beginDate,
                                    endingDate: endingDate
                                });
                                logger.info(name + ' s\'est connecté(e) à ' + myDate.toLocaleString() + ' ' + ip);
                            } else {
                                res.render('./Mobile/indexMobile1', {
                                    name: name,
                                    formationName: formationName,
                                    formationPlace: formationPlace,
                                    beginDate: beginDate,
                                    endingDate: endingDate
                                });
                                logger.info(name + ' s\'est connecté(e) à ' + myDate.toLocaleString() + ' ' + ip);
                            }
                        }
                    }
                });
            }
        }
    });
    //  } else {
    //    sess.views = 1;

    //}
})

//Page de dépot des avis via système d'étoile
app.get('/index2', function (req, res) {
    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('index2');
    } else {
        res.render('./Mobile/indexMobile2');
    }
})

//Lors de la validation, les avis via étoiles sont récupéré et stocké dans des variables
app.post('/envoiInfo1', function (req, res) {

    avis1 = req.body.avis1;
    avis2 = req.body.avis2;
    avis3 = req.body.avis3;
    avis4 = req.body.avis4;
    avis5 = req.body.avis5;
    avis6 = req.body.avis6;
    avisGeneral = req.body.avisGeneral;

    //Envoie de la page de dépot des commentaires
    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('index3');
    } else {
        res.render('./Mobile/indexMobile3');
    }

});

//Lors de la validation de la page des commentaires, on créer des variables où seront stocké les avis en étoiles pour afficher une phrase à la place
app.post('/envoiInfo2', function (req, res) {

    //Les variables du commentaire et de son titre
    titreComm = req.body.titreCommentaire;
    comm = req.body.commentaire;

    if (avis1 == 1) {
        avis1Phrase = 'Très insatisfait';
    }
    if (avis1 == 2) {
        avis1Phrase = 'Insatisfait';
    }
    if (avis1 == 3) {
        avis1Phrase = 'Moyen';
    }
    if (avis1 == 4) {
        avis1Phrase = 'Satisfait';
    }
    if (avis1 == 5) {
        avis1Phrase = 'Très satisfait';
    }
    if (avis2 == 1) {
        avis2Phrase = 'Très insatisfait';
    }

    if (avis2 == 2) {
        avis2Phrase = 'Insatisfait';
    }
    if (avis2 == 3) {
        avis2Phrase = 'Moyen';
    }
    if (avis2 == 4) {
        avis2Phrase = 'Satisfait';
    }
    if (avis2 == 5) {
        avis2Phrase = 'Très satisfait';
    }

    if (avis3 == 1) {
        avis3Phrase = 'Très insatisfait';
    }
    if (avis3 == 2) {
        avis3Phrase = 'Insatisfait';
    }
    if (avis3 == 3) {
        avis3Phrase = 'Moyen';
    }
    if (avis3 == 4) {
        avis3Phrase = 'Satisfait';
    }
    if (avis3 == 5) {
        avis3Phrase = 'Très satisfait';
    }

    if (avis4 == 1) {
        avis4Phrase = 'Très insatisfait';
    }
    if (avis4 == 2) {
        avis4Phrase = 'Insatisfait';
    }
    if (avis4 == 3) {
        avis4Phrase = 'Moyen';
    }
    if (avis4 == 4) {
        avis4Phrase = 'Satisfait';
    }
    if (avis4 == 5) {
        avis4Phrase = 'Très satisfait';
    }

    if (avis5 == 1) {
        avis5Phrase = 'Très insatisfait';
    }
    if (avis5 == 2) {
        avis5Phrase = 'Insatisfait';
    }
    if (avis5 == 3) {
        avis5Phrase = 'Moyen';
    }
    if (avis5 == 4) {
        avis5Phrase = 'Satisfait';
    }
    if (avis5 == 5) {
        avis5Phrase = 'Très satisfait';
    }

    if (avis6 == 1) {
        avis6Phrase = 'Très insatisfait';
    }
    if (avis6 == 2) {
        avis6Phrase = 'Insatisfait';
    }
    if (avis6 == 3) {
        avis6Phrase = 'Moyen';
    }
    if (avis6 == 4) {
        avis6Phrase = 'Satisfait';
    }
    if (avis6 == 5) {
        avis6Phrase = 'Très satisfait';
    }

    if (avisGeneral == 1) {
        avisGeneralPhrase = 'Très insatisfait';
    }
    if (avisGeneral == 2) {
        avisGeneralPhrase = 'Insatisfait';
    }
    if (avisGeneral == 3) {
        avisGeneralPhrase = 'Moyen';
    }
    if (avisGeneral == 4) {
        avisGeneralPhrase = 'Satisfait';
    }
    if (avisGeneral == 5) {
        avisGeneralPhrase = 'Très satisfait';
    }


    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('index4', {
            avis1Phrase: avis1Phrase,
            avis2Phrase: avis2Phrase,
            avis3Phrase: avis3Phrase,
            avis4Phrase: avis4Phrase,
            avis5Phrase: avis5Phrase,
            avis6Phrase: avis6Phrase,
            avisGeneralPhrase: avisGeneralPhrase,
            titreComm: titreComm,
            comm: comm
        });
    } else {
        res.render('./Mobile/indexMobile4');
    }

})

//Quand la page de récapitulation est validée
app.post('/envoiInfo3', function (req, res) {

    //On stock le pseudo et l'accord dans des variables
    pseudo = req.body.pseudo;
    accord = req.body.accord;
    validation = 0;

    //Si L'utilisateur n'a rien mis dans le champ 'titre du commentaire' et 'commentaire', la variable 'validation' est de niveau 3 (donc accepté)
    if (titreComm == '' && comm == '') {
        validation = 3;
    }

    //On lance une recherche des insultes contenu dans la variable 'insulte' sur le titre du commentaire, du commentaire et du pseudo laissé par l'utilisateur
    var o = pseudo.search(insulte);
    var p = titreComm.search(insulte);
    var n = comm.search(insulte);
    if (n >= 0 || o >= 0 || p >= 0) {

        //Si une insulte est trouvée
        logger.info('Une personne a mis une insulte');
        if (req.device.type.toUpperCase() == "DESKTOP") {
            res.render('refus');
        } else {
            res.render('./Mobile/refusMobile');
        }
    } else {
        logger.info('Première étape de modération : OK');

        //Si il n'y a rien dans la variable du commentaire
        if (comm == '') {
            comm = false;
        }

        //On note tout dans le fichier log
        logger.info('--------------------');
        logger.info('nom : ' + name);
        logger.info('nom de la formation : ' + formationName);
        logger.info('Lieu de la formation : ' + formationPlace);
        logger.info('Date de début : ' + beginDate);
        logger.info('Premier avis : ' + avis1);
        logger.info('Second avis : ' + avis2);
        logger.info('Troisième avis : ' + avis3);
        logger.info('Quatrième avis : ' + avis4);
        logger.info('Cinquième avis : ' + avis5);
        logger.info('Sixième avis : ' + avis6);
        logger.info('Avis Général : ' + avisGeneral);
        logger.info('Pseudo : ' + pseudo);
        logger.info('Titre commentaire : ' + titreComm);
        logger.info('Commentaire : ' + comm);
        logger.info('Accord de contacte : ' + accord);
        logger.info('date : ' + myDate);
        logger.info('Validation : ' + validation);
        logger.info('--------------------');

        //connection à MongoDB
        MongoClient.connect(url, function (err, db) {
            if (err) {
                logger.warn('Unable to connect to the mongoDB server. Error:', err);
            } else {

                //On regarde une seconde fois si l'utilisateur n'a pas déjà laissé son avis sur sa formation
                db.collection('temp').find({
                    name: name,
                    beginDate: beginDate
                }).count(function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        if (result > 0) {
                            logger.warn(name + ' a déjà donné son avis a tenté de le refaire');
                            res.render('accesDenied');
                        } else {

                            //Si tout est correct, on insert son avis dans une base de donnée Mongo
                            db.collection('temp').insert({
                                name: name,
                                formationName: formationName,
                                formationPlace: formationPlace,
                                beginDate: beginDate,
                                Pseudo: pseudo,
                                TitreCommentaire: titreComm,
                                Commentaire: comm,
                                Avis01: parseFloat(avis1),
                                Avis02: parseFloat(avis2),
                                Avis03: parseFloat(avis3),
                                Avis04: parseFloat(avis4),
                                Avis05: parseFloat(avis5),
                                Avis06: parseFloat(avis6),
                                AvisGeneral: parseFloat(avisGeneral),
                                Date: myDate.toLocaleString(),
                                Validation: validation,
                                Accord: accord
                            });

                            var myDate2 = new Date;

                            //On note dans le fichier log la date et l'heure du dépot d'avis
                            logger.info(name + ' a laissé son avis le ' + myDate2.toLocaleString());
                            if (req.device.type.toUpperCase() == "DESKTOP") {

                                //Page pour le remercier de son dépot
                                res.render('index5');
                            } else {
                                res.render('./Mobile/indexMobile5');
                            }
                        }
                    }
                });
            }
        });
    }

    /*if (req.device.type.toUpperCase() == "DESKTOP") {*/
    //res.render('index5');
    //} else {
    //  res.render('./Mobile/indexMobile5');
    //}

})

app.get('/admin', function (req, res) {
    res.render('admin');
})

app.get('/validation', function (req, res) {
    res.render('validation');
})

app.get('/supprime', function (req, res) {
    res.render('supprime');
})

app.get('/contact', function (req, res) {
    res.render('contact');
})

app.get('/apiLBF', function (req, res) {
    res.render('apiLBF');
})


//---------------------------------test--------------------------------//



//----------------------------Get et post-------------------------------------//

//Lire nouveaux commentaires
app.get('/api/affiche0', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 0
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//Lire commentaires validés
app.get('/api/affiche1', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 1
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//Lire commentaires supprimés
app.get('/api/affiche2', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 2
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });

        }
    });
})

//Supprimé
app.get('/delete/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/admin');
        }
    });
});

app.get('/delete2/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/validation');
        }
    });
});

app.get('/delete3/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/supprime');
        }
    });
});

//Validé
app.get('/validate/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/admin');
        }
    });
});

app.get('/validate2/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/validation');
        }
    });
});

app.get('/validate3/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/supprime');
        }
    });
});

//envoi la moyenne
app.get('/api/moyenne', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').aggregate([
                {
                    $match: {
                        Validation: 1
                    }
                },
                {
                    $group: {
                        _id: null,
                        moyenne: {
                            $avg: '$AvisGeneral'
                        }
                    }
                }
            ]).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//envoi la moyenne des 3 première questions sur la formatio
/*app.get('/api/moyenneFormation', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').aggregate([
                {
                    $match: {
                        Validation : 1
                    }
                },
                {
                    $group: {
                        _id: null,
                        moyenne: {
                            $avg: '$Avis01', '$Avis02', '$Avis03'
                        }
                    }
                }
            ]).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})*/



var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});