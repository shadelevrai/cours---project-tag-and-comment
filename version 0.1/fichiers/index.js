$(function () {
    
    var tag = $('#tag'), comm = $('#comm'), champ = $('.champ');
    
    tag.blur(function(){
        if($(this).val().length < 4) {
            $(this).css({
                borderColor:'red'
            })
            $('.firstDivError').text('Votre pseudo doit contenir au moins 4 charactères')
        } else {
            $(this).css({
                borderColor : 'green'
            })
            $('.firstDivError').text('');
        }
    })
    
    comm.blur(function(){
        if($(this).val().length < 20) {
            $(this).css({
                borderColor:'red'
            })
            $('.secondDivError').text('Votre commentaire doit contenir au moins 20 charactères')
        } else {
            $(this).css({
                borderColor : 'green'
            })
            $('.secondDivError').text('');
        }
    })
    
})
