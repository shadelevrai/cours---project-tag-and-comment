$(function () {

    /********************************page 01*********************************/

    $('.suivant').click(function () {
        window.open('/index2', '_self', false);
    })

    /********************************page 02**********************************/


    var reponse1 = false;
    var reponse2 = false;
    var reponse3 = false;
    var reponse4 = false;
    var reponse5 = false;
    var reponse6 = false;
    var reponse7 = false;

    //Quand la souris entre, un aperçu est disponible
    $('#1_1').mouseenter(function () {
        $('.appreciation1').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_2').mouseenter(function () {
        $('.appreciation1').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_3').mouseenter(function () {
        $('.appreciation1').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_4').mouseenter(function () {
        $('.appreciation1').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_5').mouseenter(function () {
        $('.appreciation1').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })


    //Quand la souris sort, l'aperçu disparait
    $('#1_1, #1_2, #1_3, #1_4, #1_5').mouseleave(function () {
        $('.appreciation1').text('');
    })

    //La validation pour l'erreur
    $('#1_1, #1_2, #1_3, #1_4, #1_5').click(function () {
        reponse1 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#1_1').click(function () {
        $('.appreciationValide1').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        
    })

    $('#1_2').click(function () {
        $('.appreciationValide1').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
       
    })

    $('#1_3').click(function () {
        $('.appreciationValide1').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        
    })

    $('#1_4').click(function () {
        $('.appreciationValide1').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
     
    })

    $('#1_5').click(function () {
        $('.appreciationValide1').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
      
    })



    //Quand la souris entre, un aperçu est disponible
    $('#2_1').mouseenter(function () {
        $('.appreciation2').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_2').mouseenter(function () {
        $('.appreciation2').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_3').mouseenter(function () {
        $('.appreciation2').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_4').mouseenter(function () {
        $('.appreciation2').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_5').mouseenter(function () {
        $('.appreciation2').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#2_1, #2_2, #2_3, #2_4, #2_5').mouseleave(function () {
        $('.appreciation2').text('');
    })

    //La validation pour l'erreur
    $('#2_1, #2_2, #2_3, #2_4, #2_5').click(function () {
        reponse2 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#2_1').click(function () {
        $('.appreciationValide2').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
     
    })

    $('#2_2').click(function () {
        $('.appreciationValide2').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
   
    })

    $('#2_3').click(function () {
        $('.appreciationValide2').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
      
    })

    $('#2_4').click(function () {
        $('.appreciationValide2').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
      
    })

    $('#2_5').click(function () {
        $('.appreciationValide2').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        
    })



    //Quand la souris entre, un aperçu est disponible
    $('#3_1').mouseenter(function () {
        $('.appreciation3').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_2').mouseenter(function () {
        $('.appreciation3').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_3').mouseenter(function () {
        $('.appreciation3').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_4').mouseenter(function () {
        $('.appreciation3').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_5').mouseenter(function () {
        $('.appreciation3').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#3_1, #3_2, #3_3, #3_4, #3_5').mouseleave(function () {
        $('.appreciation3').text('');
    })

    //La validation pour l'erreur
    $('#3_1, #3_2, #3_3, #3_4, #3_5').click(function () {
        reponse3 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#3_1').click(function () {
        $('.appreciationValide3').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
       
    })

    $('#3_2').click(function () {
        $('.appreciationValide3').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
      
    })

    $('#3_3').click(function () {
        $('.appreciationValide3').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
       
    })

    $('#3_4').click(function () {
        $('.appreciationValide3').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
    
    })

    $('#3_5').click(function () {
        $('.appreciationValide3').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
      
    })


    //Quand la souris entre, un aperçu est disponible
    $('#4_1').mouseenter(function () {
        $('.appreciation4').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_2').mouseenter(function () {
        $('.appreciation4').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_3').mouseenter(function () {
        $('.appreciation4').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_4').mouseenter(function () {
        $('.appreciation4').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_5').mouseenter(function () {
        $('.appreciation4').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#4_1, #4_2, #4_3, #4_4, #4_5').mouseleave(function () {
        $('.appreciation4').text('');
    })

    //La validation pour l'erreur
    $('#4_1, #4_2, #4_3, #4_4, #4_5').click(function () {
        reponse4 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#4_1').click(function () {
        $('.appreciationValide4').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
     
    })

    $('#4_2').click(function () {
        $('.appreciationValide4').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
       
    })

    $('#4_3').click(function () {
        $('.appreciationValide4').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
       
    })

    $('#4_4').click(function () {
        $('.appreciationValide4').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
      
    })

    $('#4_5').click(function () {
        $('.appreciationValide4').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
      
    })


    //Quand la souris entre, un aperçu est disponible
    $('#5_1').mouseenter(function () {
        $('.appreciation5').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_2').mouseenter(function () {
        $('.appreciation5').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_3').mouseenter(function () {
        $('.appreciation5').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_4').mouseenter(function () {
        $('.appreciation5').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_5').mouseenter(function () {
        $('.appreciation5').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#5_1, #5_2, #5_3, #5_4, #5_5').mouseleave(function () {
        $('.appreciation5').text('');
    })

    //La validation pour l'erreur
    $('#5_1, #5_2, #5_3, #5_4, #5_5').click(function () {
        reponse5 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#5_1').click(function () {
        $('.appreciationValide5').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
     
    })

    $('#5_2').click(function () {
        $('.appreciationValide5').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
      
    })

    $('#5_3').click(function () {
        $('.appreciationValide5').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
     
    })

    $('#5_4').click(function () {
        $('.appreciationValide5').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
      
    })

    $('#5_5').click(function () {
        $('.appreciationValide5').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
     
    })


    //Quand la souris entre, un aperçu est disponible
    $('#6_1').mouseenter(function () {
        $('.appreciation6').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_2').mouseenter(function () {
        $('.appreciation6').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_3').mouseenter(function () {
        $('.appreciation6').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_4').mouseenter(function () {
        $('.appreciation6').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_5').mouseenter(function () {
        $('.appreciation6').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#6_1, #6_2, #6_3, #6_4, #6_5').mouseleave(function () {
        $('.appreciation6').text('');
    })

    //La validation pour l'erreur
    $('#6_1, #6_2, #6_3, #6_4, #6_5').click(function () {
        reponse6 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#6_1').click(function () {
        $('.appreciationValide6').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
     
    })

    $('#6_2').click(function () {
        $('.appreciationValide6').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
       
    })

    $('#6_3').click(function () {
        $('.appreciationValide6').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
     
    })

    $('#6_4').click(function () {
        $('.appreciationValide6').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
    
    })

    $('#6_5').click(function () {
        $('.appreciationValide6').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
     
    })


    //Quand la souris entre, un aperçu est disponible
    $('#7_1').mouseenter(function () {
        $('.appreciation7').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_2').mouseenter(function () {
        $('.appreciation7').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_3').mouseenter(function () {
        $('.appreciation7').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_4').mouseenter(function () {
        $('.appreciation7').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_5').mouseenter(function () {
        $('.appreciation7').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#7_1, #7_2, #7_3, #7_4, #7_5').mouseleave(function () {
        $('.appreciation7').text('');
    })

    //La validation pour l'erreur
    $('#7_1, #7_2, #7_3, #7_4, #7_5').click(function () {
        reponse7 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#7_1').click(function () {
        $('.appreciationValide7').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
       
    })

    $('#7_2').click(function () {
        $('.appreciationValide7').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        
    })

    $('#7_3').click(function () {
        $('.appreciationValide7').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
      
    })

    $('#7_4').click(function () {
        $('.appreciationValide7').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
      
    })

    $('#7_5').click(function () {
        $('.appreciationValide7').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
    
    })



    $('.retour, .retour2').click(function () {
        window.open('/index2', '_self', false);
    });

    var tag = $('#tag');

    tag.keydown(function () {
        $('.nombreCharactere').text('Nombre de charactères restant : ' + (200 - $(this).val().length)).css({
            font: 'calibri',
            fontSize: '14px',
            marginLeft: '28px',
            marginTop: '10px'
        });
    })


    //Apparition de l'aide quand la souris entre
    $('.aide1').click(function () {
        $('.texteAide1').toggle();
    })

    $('.aide2').click(function () {
        $('.texteAide2').toggle();
    })

    $('.aide3').click(function () {
        $('.texteAide3').toggle();
    })

    $('.aide4').click(function () {
        $('.texteAide4').toggle();
    })


    $('.etapeSuivante').click(function (event) {
        if (reponse1 == false || reponse2 == false || reponse3 == false || reponse4 == false || reponse5 == false || reponse6 == false || reponse7 == false) {
            event.preventDefault();
        }
        if (reponse1 == false) {
            $('.appreciation1').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse2 == false) {
            $('.appreciation2').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse3 == false) {
            $('.appreciation3').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse4 == false) {
            $('.appreciation4').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse5 == false) {
            $('.appreciation5').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse6 == false) {
            $('.appreciation6').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }

        if (reponse7 == false) {
            $('.appreciation7').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }

    });


    $('.suivant6').click(function (event) {
        if ($('.pseudo').val() == '') {
            event.preventDefault();
            $('.manqueLePseudo').text('Vous avez oublié de mettre un pseudo').css({
                color: 'palevioletred',
                marginLeft: '40px'
            })
        } else {
            console.log('ok');
        }
    })



    if ($('.recap1').text() == 'Très insatisfait') {
        $('.recap1').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/10star.png');
    }
    
    $('#recap01').css({
        marginTop:'px'
    });
    if ($('.recap1').text() == 'Insatisfait') {
        $('.recap1').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/20star.png');
    }
    if ($('.recap1').text() == 'Moyen') {
        $('.recap1').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/30star.png');
    }
    if ($('.recap1').text() == 'Satisfait') {
        $('.recap1').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/40star.png');
    }
    if ($('.recap1').text() == 'Très satisfait') {
        $('.recap1').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/50star.png');
    }

    if ($('.recap2').text() == 'Très insatisfait') {
        $('.recap2').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star2').attr('src', 'img/10star.png');
    }
    if ($('.recap2').text() == 'Insatisfait') {
        $('.recap2').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star2').attr('src', 'img/20star.png');
    }
    if ($('.recap2').text() == 'Moyen') {
        $('.recap2').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star2').attr('src', 'img/30star.png');
    }
    if ($('.recap2').text() == 'Satisfait') {
        $('.recap2').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star2').attr('src', 'img/40star.png');
    }
    if ($('.recap2').text() == 'Très satisfait') {
        $('.recap2').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star2').attr('src', 'img/50star.png');
    }

    if ($('.recap3').text() == 'Très insatisfait') {
        $('.recap3').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star3').attr('src', 'img/10star.png');
    }
    if ($('.recap3').text() == 'Insatisfait') {
        $('.recap3').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star3').attr('src', 'img/20star.png');
    }
    if ($('.recap3').text() == 'Moyen') {
        $('.recap3').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star3').attr('src', 'img/30star.png');
    }
    if ($('.recap3').text() == 'Satisfait') {
        $('.recap3').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star3').attr('src', 'img/40star.png');
    }
    if ($('.recap3').text() == 'Très satisfait') {
        $('.recap3').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star3').attr('src', 'img/50star.png');
    }

    if ($('.recap4').text() == 'Très insatisfait') {
        $('.recap4').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star4').attr('src', 'img/10star.png');
    }
    if ($('.recap4').text() == 'Insatisfait') {
        $('.recap4').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star4').attr('src', 'img/20star.png');
    }
    if ($('.recap4').text() == 'Moyen') {
        $('.recap4').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star4').attr('src', 'img/30star.png');
    }
    if ($('.recap4').text() == 'Satisfait') {
        $('.recap4').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star4').attr('src', 'img/40star.png');
    }
    if ($('.recap4').text() == 'Très satisfait') {
        $('.recap4').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star4').attr('src', 'img/50star.png');
    }

    if ($('.recap5').text() == 'Très insatisfait') {
        $('.recap5').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star5').attr('src', 'img/10star.png');
    }
    if ($('.recap5').text() == 'Insatisfait') {
        $('.recap5').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star5').attr('src', 'img/20star.png');
    }
    if ($('.recap5').text() == 'Moyen') {
        $('.recap5').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star5').attr('src', 'img/30star.png');
    }
    if ($('.recap5').text() == 'Satisfait') {
        $('.recap5').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star5').attr('src', 'img/40star.png');
    }
    if ($('.recap5').text() == 'Très satisfait') {
        $('.recap5').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star5').attr('src', 'img/50star.png');
    }

    if ($('.recap6').text() == 'Très insatisfait') {
        $('.recap6').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star6').attr('src', 'img/10star.png');
    }
    if ($('.recap6').text() == 'Insatisfait') {
        $('.recap6').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star6').attr('src', 'img/20star.png');
    }
    if ($('.recap6').text() == 'Moyen') {
        $('.recap6').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star6').attr('src', 'img/30star.png');
    }
    if ($('.recap6').text() == 'Satisfait') {
        $('.recap6').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star6').attr('src', 'img/40star.png');
    }
    if ($('.recap6').text() == 'Très satisfait') {
        $('.recap6').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star6').attr('src', 'img/50star.png');
    }

    if ($('.recapGenerale').text() == 'Très insatisfait') {
        $('.recapGenerale').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star7').attr('src', 'img/10star.png');
    }
    if ($('.recapGenerale').text() == 'Insatisfait') {
        $('.recapGenerale').css({
            color: 'red',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star7').attr('src', 'img/20star.png');
    }
    if ($('.recapGenerale').text() == 'Moyen') {
        $('.recapGenerale').css({
            color: 'orange',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star7').attr('src', 'img/30star.png');
    }
    if ($('.recapGenerale').text() == 'Satisfait') {
        $('.recapGenerale').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star7').attr('src', 'img/40star.png');
    }
    if ($('.recapGenerale').text() == 'Très satisfait') {
        $('.recapGenerale').css({
            color: 'green',
            marginTop: '5px',
            fontSize: '12px'
        });
        $('.star7').attr('src', 'img/50star.png');
    }


});