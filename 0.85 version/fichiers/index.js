//fonction pour récupérer les parametres du lien coté client
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


//prendre les parametre et les stocker dans les cookies
if (window.location.pathname === '/') {
    var idSecret = getParameterByName('idsecret'),
        nomStagiaire = getParameterByName('nomstagiaire'),
        prenomStagiaire = getParameterByName('prenomstagiaire'),
        intituleFormation = getParameterByName('intituleformation'),
        lieuFormation = getParameterByName('lieuformation'),
        dateDebut = getParameterByName('datedebut'),
        dateFin = getParameterByName('datefin'),
        idCarifSession = getParameterByName('idcarifsession'),
        idCarifAction = getParameterByName('idcarifaction'),
        idCarifFormation = getParameterByName('idcarifformation'),
        financement = getParameterByName('financement'),
        formacode = getParameterByName('formacode'),
        codeCertifInfo = getParameterByName('codecertifinfo'),
        idCarifOrganisme = getParameterByName('idcariforganisme'),
        nomOrganisme = getParameterByName('nomorganisme'),
        adresseOrganisme = getParameterByName('adresseorganisme'),
        mailOrganisme = getParameterByName('mailorganisme'),
        siren = getParameterByName('siren'),
        siret = getParameterByName('siret');

    document.cookie = 'idSecret=' + idSecret;
    document.cookie = 'nomStagiaire=' + nomStagiaire;
    document.cookie = 'prenomStagiaire=' + prenomStagiaire;
    document.cookie = 'intituleFormation=' + intituleFormation;
    document.cookie = 'lieuFormation=' + lieuFormation;
    document.cookie = 'dateDebut=' + dateDebut;
    document.cookie = 'dateFin=' + dateFin;
    document.cookie = 'idCarifSession=' + idCarifSession;
    document.cookie = 'idCarifAction=' + idCarifAction;
    document.cookie = 'idCarifFormation=' + idCarifFormation;
    document.cookie = 'financement=' + financement;
    document.cookie = 'formacode=' + formacode;
    document.cookie = 'codeCertifInfo=' + codeCertifInfo;
    document.cookie = 'idCarifOrganisme=' + idCarifOrganisme;
    document.cookie = 'nomOrganisme=' + nomOrganisme;
    document.cookie = 'adresseOrganisme=' + adresseOrganisme;
    document.cookie = 'mailOrganisme=' + mailOrganisme;
    document.cookie = 'siren=' + siren;
    document.cookie = 'nomStagiaire=' + nomStagiaire;
    document.cookie = 'siret=' + siret;

    document.getElementById('prenom').innerHTML = prenomStagiaire;
    document.getElementById('nom').innerHTML = nomStagiaire;

    document.getElementById('formationB').innerHTML = intituleFormation;
    document.getElementById('formationC').innerHTML = lieuFormation;

    document.getElementById('formationD').innerHTML = dateDebut;
    document.getElementById('formationE').innerHTML = dateFin;

}

//fonction pour lire les cookies
function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

if (window.location.pathname === '/envoiInfo2') {
    document.getElementById('nom').innerHTML = getCookie('intituleFormation');
}

$(function () {



    /********************************page 01*********************************/

    $('.suivant').click(function () {
        window.open('/index2', '_self', false);
    })

    /********************************page 02**********************************/


    var reponse1 = false;
    var reponse2 = false;
    var reponse3 = false;
    var reponse4 = false;
    var reponse5 = false;
    var reponse6 = false;
    var reponse7 = false;

    //Quand la souris entre, un aperçu est disponible
    $('#1_1').mouseenter(function () {
        $('.appreciation1').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_2').mouseenter(function () {
        $('.appreciation1').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_3').mouseenter(function () {
        $('.appreciation1').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_4').mouseenter(function () {
        $('.appreciation1').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#1_5').mouseenter(function () {
        $('.appreciation1').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })


    //Quand la souris sort, l'aperçu disparait
    $('#1_1, #1_2, #1_3, #1_4, #1_5').mouseleave(function () {
        $('.appreciation1').text('');
    })

    //La validation pour l'erreur
    $('#1_1, #1_2, #1_3, #1_4, #1_5').click(function () {
        reponse1 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#1_1').click(function () {
        $('.appreciationValide1').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        document.cookie = 'accueil=1';
    })

    $('#1_2').click(function () {
        $('.appreciationValide1').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        document.cookie = 'accueil=2';
    })

    $('#1_3').click(function () {
        $('.appreciationValide1').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        document.cookie = 'accueil=3';

    })

    $('#1_4').click(function () {
        $('.appreciationValide1').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        document.cookie = 'accueil=4';

    })

    $('#1_5').click(function () {
        $('.appreciationValide1').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation1').text('');
        document.cookie = 'accueil=5';

    })



    //Quand la souris entre, un aperçu est disponible
    $('#2_1').mouseenter(function () {
        $('.appreciation2').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_2').mouseenter(function () {
        $('.appreciation2').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_3').mouseenter(function () {
        $('.appreciation2').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_4').mouseenter(function () {
        $('.appreciation2').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#2_5').mouseenter(function () {
        $('.appreciation2').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#2_1, #2_2, #2_3, #2_4, #2_5').mouseleave(function () {
        $('.appreciation2').text('');
    })

    //La validation pour l'erreur
    $('#2_1, #2_2, #2_3, #2_4, #2_5').click(function () {
        reponse2 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#2_1').click(function () {
        $('.appreciationValide2').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        document.cookie = 'suivi=1';
    })

    $('#2_2').click(function () {
        $('.appreciationValide2').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        document.cookie = 'suivi=2';
    })

    $('#2_3').click(function () {
        $('.appreciationValide2').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        document.cookie = 'suivi=3';
    })

    $('#2_4').click(function () {
        $('.appreciationValide2').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        document.cookie = 'suivi=4';
    })

    $('#2_5').click(function () {
        $('.appreciationValide2').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation2').text('');
        document.cookie = 'suivi=5';
    })



    //Quand la souris entre, un aperçu est disponible
    $('#3_1').mouseenter(function () {
        $('.appreciation3').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_2').mouseenter(function () {
        $('.appreciation3').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_3').mouseenter(function () {
        $('.appreciation3').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_4').mouseenter(function () {
        $('.appreciation3').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#3_5').mouseenter(function () {
        $('.appreciation3').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#3_1, #3_2, #3_3, #3_4, #3_5').mouseleave(function () {
        $('.appreciation3').text('');
    })

    //La validation pour l'erreur
    $('#3_1, #3_2, #3_3, #3_4, #3_5').click(function () {
        reponse3 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#3_1').click(function () {
        $('.appreciationValide3').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
        document.cookie = 'equipe=1';
    })

    $('#3_2').click(function () {
        $('.appreciationValide3').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
        document.cookie = 'equipe=2';
    })

    $('#3_3').click(function () {
        $('.appreciationValide3').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
        document.cookie = 'equipe=3';
    })

    $('#3_4').click(function () {
        $('.appreciationValide3').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
        document.cookie = 'equipe=4';
    })

    $('#3_5').click(function () {
        $('.appreciationValide3').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation3').text('');
        document.cookie = 'equipe=5';
    })


    //Quand la souris entre, un aperçu est disponible
    $('#4_1').mouseenter(function () {
        $('.appreciation4').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_2').mouseenter(function () {
        $('.appreciation4').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_3').mouseenter(function () {
        $('.appreciation4').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_4').mouseenter(function () {
        $('.appreciation4').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#4_5').mouseenter(function () {
        $('.appreciation4').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#4_1, #4_2, #4_3, #4_4, #4_5').mouseleave(function () {
        $('.appreciation4').text('');
    })

    //La validation pour l'erreur
    $('#4_1, #4_2, #4_3, #4_4, #4_5').click(function () {
        reponse4 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#4_1').click(function () {
        $('.appreciationValide4').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
        document.cookie = 'moyen=1';
    })

    $('#4_2').click(function () {
        $('.appreciationValide4').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
        document.cookie = 'moyen=2'
    })

    $('#4_3').click(function () {
        $('.appreciationValide4').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
        document.cookie = 'moyen=3'
    })

    $('#4_4').click(function () {
        $('.appreciationValide4').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
        document.cookie = 'moyen=4'
    })

    $('#4_5').click(function () {
        $('.appreciationValide4').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation4').text('');
        document.cookie = 'moyen=5'
    })


    //Quand la souris entre, un aperçu est disponible
    $('#5_1').mouseenter(function () {
        $('.appreciation5').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_2').mouseenter(function () {
        $('.appreciation5').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_3').mouseenter(function () {
        $('.appreciation5').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_4').mouseenter(function () {
        $('.appreciation5').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#5_5').mouseenter(function () {
        $('.appreciation5').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#5_1, #5_2, #5_3, #5_4, #5_5').mouseleave(function () {
        $('.appreciation5').text('');
    })

    //La validation pour l'erreur
    $('#5_1, #5_2, #5_3, #5_4, #5_5').click(function () {
        reponse5 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#5_1').click(function () {
        $('.appreciationValide5').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
        document.cookie = 'accompagnement=1'
    })

    $('#5_2').click(function () {
        $('.appreciationValide5').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
        document.cookie = 'accompagnement=2'
    })

    $('#5_3').click(function () {
        $('.appreciationValide5').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
        document.cookie = 'accompagnement=3'
    })

    $('#5_4').click(function () {
        $('.appreciationValide5').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
        document.cookie = 'accompagnement=4'
    })

    $('#5_5').click(function () {
        $('.appreciationValide5').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation5').text('');
        document.cookie = 'accompagnement=5'
    })


    //Quand la souris entre, un aperçu est disponible
    $('#6_1').mouseenter(function () {
        $('.appreciation6').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_2').mouseenter(function () {
        $('.appreciation6').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_3').mouseenter(function () {
        $('.appreciation6').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_4').mouseenter(function () {
        $('.appreciation6').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#6_5').mouseenter(function () {
        $('.appreciation6').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#6_1, #6_2, #6_3, #6_4, #6_5').mouseleave(function () {
        $('.appreciation6').text('');
    })

    //La validation pour l'erreur
    $('#6_1, #6_2, #6_3, #6_4, #6_5').click(function () {
        reponse6 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#6_1').click(function () {
        $('.appreciationValide6').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
        document.cookie = 'recommandation=1'
    })

    $('#6_2').click(function () {
        $('.appreciationValide6').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
        document.cookie = 'recommandation=2'
    })

    $('#6_3').click(function () {
        $('.appreciationValide6').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
        document.cookie = 'recommandation=3'
    })

    $('#6_4').click(function () {
        $('.appreciationValide6').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
        document.cookie = 'recommandation=4'
    })

    $('#6_5').click(function () {
        $('.appreciationValide6').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation6').text('');
        document.cookie = 'recommandation=5'
    })


    //Quand la souris entre, un aperçu est disponible
    $('#7_1').mouseenter(function () {
        $('.appreciation7').text('Très insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_2').mouseenter(function () {
        $('.appreciation7').text('Insatisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_3').mouseenter(function () {
        $('.appreciation7').text('Moyen').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_4').mouseenter(function () {
        $('.appreciation7').text('Satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    $('#7_5').mouseenter(function () {
        $('.appreciation7').text('Très satisfait').css({
            color: 'black',
            fontSize: '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#7_1, #7_2, #7_3, #7_4, #7_5').mouseleave(function () {
        $('.appreciation7').text('');
    })

    //La validation pour l'erreur
    $('#7_1, #7_2, #7_3, #7_4, #7_5').click(function () {
        reponse7 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#7_1').click(function () {
        $('.appreciationValide7').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        document.cookie = 'general=1'
    })

    $('#7_2').click(function () {
        $('.appreciationValide7').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        document.cookie = 'general=2'
    })

    $('#7_3').click(function () {
        $('.appreciationValide7').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        document.cookie = 'general=3'
    })

    $('#7_4').click(function () {
        $('.appreciationValide7').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        document.cookie = 'general=4'
    })

    $('#7_5').click(function () {
        $('.appreciationValide7').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.appreciation7').text('');
        document.cookie = 'general=5'
    })



    $('.retour, .retour2').click(function () {
        window.open('/index2', '_self', false);
    });



    //Apparition de l'aide quand la souris entre
    $('.aide1').click(function () {
        $('.texteAide1').toggle();
    })

    $('.aide2').click(function () {
        $('.texteAide2').toggle();
    })

    $('.aide3').click(function () {
        $('.texteAide3').toggle();
    })

    $('.aide4').click(function () {
        $('.texteAide4').toggle();
    })


    $('.etapeSuivante').click(function (event) {
        if (reponse1 == false || reponse2 == false || reponse3 == false || reponse4 == false || reponse5 == false || reponse6 == false || reponse7 == false) {
            event.preventDefault();
        }
        if (reponse1 == false) {
            $('.appreciation1').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse2 == false) {
            $('.appreciation2').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse3 == false) {
            $('.appreciation3').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse4 == false) {
            $('.appreciation4').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse5 == false) {
            $('.appreciation5').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }
        if (reponse6 == false) {
            $('.appreciation6').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }

        if (reponse7 == false) {
            $('.appreciation7').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });

        }

    });



    var tag = $('#tag');

    tag.keydown(function () {
        $('.nombreCharactere').text('Nombre de charactères restant : ' + (200 - $(this).val().length)).css({
            font: 'calibri',
            fontSize: '14px',
            marginLeft: '28px',
            marginTop: '10px'
        });
    })

    $('.suivant5').click(function () {
        document.cookie = 'commentaire=' + tag.val();
        document.cookie = 'titreCommentaire=' + $('.titreComm').val();
    })



    $('.suivant6').click(function (event) {
        if ($('.pseudo').val() == '') {
            event.preventDefault();
            $('.manqueLePseudo').text('Vous avez oublié de mettre un pseudo').css({
                color: 'palevioletred',
                marginLeft: '40px'
            })
        } else {
            console.log('ok');
            //var idSecretStore = sessionStorage.getItem("idSecret");
            //console.log(idSecretStore);
            //exports.idSecretStore = idSecretStore;
        }
    })



    if (getCookie('accueil') === '1') {
        $('.recap1').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.star1').attr('src', 'img/10star.png');
        $('.recap1').text('Très insatisfait');
    }

    $('#recap01').css({
        marginTop: 'px'
    });
    if (getCookie('accueil') === '2') {
        $('.recap1').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap1').text('Insatisfait');
        $('.star1').attr('src', 'img/20star.png');
    }
    if (getCookie('accueil') === '3') {
        $('.recap1').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap1').text('Moyen');
        $('.star1').attr('src', 'img/30star.png');
    }
    if (getCookie('accueil') === '4') {
        $('.recap1').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap1').text('Satisfait');
        $('.star1').attr('src', 'img/40star.png');
    }
    if (getCookie('accueil') === '5') {
        $('.recap1').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap1').text('Très satisfait');
        $('.star1').attr('src', 'img/50star.png');
    }

    if (getCookie('suivi') === '1') {
        $('.recap2').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap2').text('Très insatisfait');
        $('.star2').attr('src', 'img/10star.png');
    }
    if (getCookie('suivi') === '2') {
        $('.recap2').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap2').text('Insatisfait');
        $('.star2').attr('src', 'img/20star.png');
    }
    if (getCookie('suivi') === '3') {
        $('.recap2').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap2').text('Moyen')
        $('.star2').attr('src', 'img/30star.png');
    }
    if (getCookie('suivi') === '4') {
        $('.recap2').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap2').text('Satisfait')
        $('.star2').attr('src', 'img/40star.png');
    }
    if (getCookie('suivi') === '5') {
        $('.recap2').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap2').text('Très satisfait');
        $('.star2').attr('src', 'img/50star.png');
    }

    if (getCookie('equipe') === '1') {
        $('.recap3').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap3').text('Très insatisfait');
        $('.star3').attr('src', 'img/10star.png');
    }
    if (getCookie('equipe') === '2') {
        $('.recap3').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap3').text('Insatisfait');
        $('.star3').attr('src', 'img/20star.png');
    }
    if (getCookie('equipe') === '3') {
        $('.recap3').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap3').text('Moyen');
        $('.star3').attr('src', 'img/30star.png');
    }
    if (getCookie('equipe') === '4') {
        $('.recap3').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap3').text('Satisfait');
        $('.star3').attr('src', 'img/40star.png');
    }
    if (getCookie('equipe') === '5') {
        $('.recap3').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap3').text('Très satisfait');
        $('.star3').attr('src', 'img/50star.png');
    }

    if (getCookie('Moyen') === '1') {
        $('.recap4').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap4').text('Très insatisfait');
        $('.star4').attr('src', 'img/10star.png');
    }
    if (getCookie('moyen') === '2') {
        $('.recap4').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap4').text('Insatisfait');
        $('.star4').attr('src', 'img/20star.png');
    }
    if (getCookie('moyen') === '3') {
        $('.recap4').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap4').text('Moyen');
        $('.star4').attr('src', 'img/30star.png');
    }
    if (getCookie('moyen') === '4') {
        $('.recap4').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap4').text('Satisfait');
        $('.star4').attr('src', 'img/40star.png');
    }
    if (getCookie('moyen') === '5') {
        $('.recap4').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap4').text('Très satisfait');
        $('.star4').attr('src', 'img/50star.png');
    }

    if (getCookie('accompagnement') === '1') {
        $('.recap5').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap5').text('Très insatisfait');
        $('.star5').attr('src', 'img/10star.png');
    }
    if (getCookie('accompagnement') === '2') {
        $('.recap5').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap5').text('Insatisfait');
        $('.star5').attr('src', 'img/20star.png');
    }
    if (getCookie('accompagnement') === '3') {
        $('.recap5').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap5').text('Moyen');
        $('.star5').attr('src', 'img/30star.png');
    }
    if (getCookie('accompagnement') === '4') {
        $('.recap5').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap5').text('Satisfait');
        $('.star5').attr('src', 'img/40star.png');
    }
    if (getCookie('accompagnement') === '5') {
        $('.recap5').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap5').text('Très satisfait');
        $('.star5').attr('src', 'img/50star.png');
    }

    if (getCookie('recommandation') === '1') {
        $('.recap6').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap6').text('Très insatisfait');
        $('.star6').attr('src', 'img/10star.png');
    }
    
    if (getCookie('recommandation') === '2') {
        $('.recap6').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap6').text('Insatisfait');
        $('.star6').attr('src', 'img/20star.png');
    }
    
    if (getCookie('recommandation') === '3') {
        $('.recap6').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap6').text('Moyen');
        $('.star6').attr('src', 'img/30star.png');
    }
    
    if (getCookie('recommandation') === '4') {
        $('.recap6').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap6').text('Satisfait');
        $('.star6').attr('src', 'img/40star.png');
    }
    
    if (getCookie('recommandation') === '5') {
        $('.recap6').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recap6').text('Très Satisfait');
        $('.star6').attr('src', 'img/50star.png');
    }

    if (getCookie('general') === '1') {
        $('.recapGenerale').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recapGenerale').text('Très insatisfait');
        $('.star7').attr('src', 'img/10star.png');
    }
    if (getCookie('general') === '2') {
        $('.recapGenerale').css({
            color: 'red',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recapGenerale').text('Insatisfait');
        $('.star7').attr('src', 'img/20star.png');
    }
    if (getCookie('general') === '3') {
        $('.recapGenerale').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recapGenerale').text('Moyen');
        $('.star7').attr('src', 'img/30star.png');
    }
    if (getCookie('general') === '4') {
        $('.recapGenerale').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recapGenerale').text('Satisfait');
        $('.star7').attr('src', 'img/40star.png');
    }
    if (getCookie('general') === '5') {
        $('.recapGenerale').css({
            color: 'green',
            marginTop: '-10px',
            fontSize: '12px'
        });
        $('.recapGenerale').text('très satisfait');
        $('.star7').attr('src', 'img/50star.png');
    }

    if (window.location.pathname === '/envoiInfo2'){
        $('#titreComm').text(getCookie('titreCommentaire'));
        $('#comm').text(getCookie('commentaire'));
    }
    
    $('.suivant4').click(function () {
        window.open('/index2', '_self', false);
    })

});




//console.log(window.location.pathname);