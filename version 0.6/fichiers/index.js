$(function () {

    /********************************page 01*********************************/

    $('.suivant').click(function () {
        window.open('/index2', '_self', false);
    })

    /********************************page 02**********************************/
    

    var reponse1 = false;
    var reponse2 = false;
    var reponse3 = false;
    var reponse4 = false;
    var reponse5 = false;
    var reponse6 = false;

    //Quand la souris entre, un aperçu est disponible
    $('#1_1').mouseenter(function () {
        $('.appreciation1').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#1_2').mouseenter(function () {
        $('.appreciation1').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#1_3').mouseenter(function () {
        $('.appreciation1').text('Moyen').css({
            color: 'black'
        });
    })

    $('#1_4').mouseenter(function () {
        $('.appreciation1').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#1_5').mouseenter(function () {
        $('.appreciation1').text('Très satisfait').css({
            color: 'black'
        });
    })


    //Quand la souris sort, l'aperçu disparait
    $('#1_1, #1_2, #1_3, #1_4, #1_5').mouseleave(function () {
        $('.appreciation1').text('');
    })

    //La validation pour l'erreur
    $('#1_1, #1_2, #1_3, #1_4, #1_5').click(function () {
        reponse1 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#1_1').click(function () {
        $('.appreciationValide1').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation1').text('');
    })

    $('#1_2').click(function () {
        $('.appreciationValide1').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation1').text('');
    })

    $('#1_3').click(function () {
        $('.appreciationValide1').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation1').text('');
    })

    $('#1_4').click(function () {
        $('.appreciationValide1').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation1').text('');
    })

    $('#1_5').click(function () {
        $('.appreciationValide1').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation1').text('');
    })



    //Quand la souris entre, un aperçu est disponible
    $('#2_1').mouseenter(function () {
        $('.appreciation2').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#2_2').mouseenter(function () {
        $('.appreciation2').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#2_3').mouseenter(function () {
        $('.appreciation2').text('Moyen').css({
            color: 'black'
        });
    })

    $('#2_4').mouseenter(function () {
        $('.appreciation2').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#2_5').mouseenter(function () {
        $('.appreciation2').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#2_1, #2_2, #2_3, #2_4, #2_5').mouseleave(function () {
        $('.appreciation2').text('');
    })

    //La validation pour l'erreur
    $('#2_1, #2_2, #2_3, #2_4, #2_5').click(function () {
        reponse2 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#2_1').click(function () {
        $('.appreciationValide2').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation2').text('');
    })

    $('#2_2').click(function () {
        $('.appreciationValide2').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation2').text('');
    })

    $('#2_3').click(function () {
        $('.appreciationValide2').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation2').text('');
    })

    $('#2_4').click(function () {
        $('.appreciationValide2').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation2').text('');
    })

    $('#2_5').click(function () {
        $('.appreciationValide2').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation2').text('');
    })



    //Quand la souris entre, un aperçu est disponible
    $('#3_1').mouseenter(function () {
        $('.appreciation3').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#3_2').mouseenter(function () {
        $('.appreciation3').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#3_3').mouseenter(function () {
        $('.appreciation3').text('Moyen').css({
            color: 'black'
        });
    })

    $('#3_4').mouseenter(function () {
        $('.appreciation3').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#3_5').mouseenter(function () {
        $('.appreciation3').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#3_1, #3_2, #3_3, #3_4, #3_5').mouseleave(function () {
        $('.appreciation3').text('');
    })

    //La validation pour l'erreur
    $('#3_1, #3_2, #3_3, #3_4, #3_5').click(function () {
        reponse3 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#3_1').click(function () {
        $('.appreciationValide3').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation3').text('');
    })

    $('#3_2').click(function () {
        $('.appreciationValide3').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation3').text('');
    })

    $('#3_3').click(function () {
        $('.appreciationValide3').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation3').text('');
    })

    $('#3_4').click(function () {
        $('.appreciationValide3').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation3').text('');
    })

    $('#3_5').click(function () {
        $('.appreciationValide3').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation3').text('');
    })


    //Quand la souris entre, un aperçu est disponible
    $('#4_1').mouseenter(function () {
        $('.appreciation4').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#4_2').mouseenter(function () {
        $('.appreciation4').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#4_3').mouseenter(function () {
        $('.appreciation4').text('Moyen').css({
            color: 'black'
        });
    })

    $('#4_4').mouseenter(function () {
        $('.appreciation4').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#4_5').mouseenter(function () {
        $('.appreciation4').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#4_1, #4_2, #4_3, #4_4, #4_5').mouseleave(function () {
        $('.appreciation4').text('');
    })

    //La validation pour l'erreur
    $('#4_1, #4_2, #4_3, #4_4, #4_5').click(function () {
        reponse4 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#4_1').click(function () {
        $('.appreciationValide4').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation4').text('');
    })

    $('#4_2').click(function () {
        $('.appreciationValide4').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation4').text('');
    })

    $('#4_3').click(function () {
        $('.appreciationValide4').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation4').text('');
    })

    $('#4_4').click(function () {
        $('.appreciationValide4').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation4').text('');
    })

    $('#4_5').click(function () {
        $('.appreciationValide4').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation4').text('');
    })


    //Quand la souris entre, un aperçu est disponible
    $('#5_1').mouseenter(function () {
        $('.appreciation5').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#5_2').mouseenter(function () {
        $('.appreciation5').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#5_3').mouseenter(function () {
        $('.appreciation5').text('Moyen').css({
            color: 'black'
        });
    })

    $('#5_4').mouseenter(function () {
        $('.appreciation5').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#5_5').mouseenter(function () {
        $('.appreciation5').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#5_1, #5_2, #5_3, #5_4, #5_5').mouseleave(function () {
        $('.appreciation5').text('');
    })

    //La validation pour l'erreur
    $('#5_1, #5_2, #5_3, #5_4, #5_5').click(function () {
        reponse5 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#5_1').click(function () {
        $('.appreciationValide5').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation5').text('');
    })

    $('#5_2').click(function () {
        $('.appreciationValide5').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation5').text('');
    })

    $('#5_3').click(function () {
        $('.appreciationValide5').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation5').text('');
    })

    $('#5_4').click(function () {
        $('.appreciationValide5').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation5').text('');
    })

    $('#5_5').click(function () {
        $('.appreciationValide5').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation5').text('');
    })


    //Quand la souris entre, un aperçu est disponible
    $('#6_1').mouseenter(function () {
        $('.appreciation6').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#6_2').mouseenter(function () {
        $('.appreciation6').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#6_3').mouseenter(function () {
        $('.appreciation6').text('Moyen').css({
            color: 'black'
        });
    })

    $('#6_4').mouseenter(function () {
        $('.appreciation6').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#6_5').mouseenter(function () {
        $('.appreciation6').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#6_1, #6_2, #6_3, #6_4, #6_5').mouseleave(function () {
        $('.appreciation6').text('');
    })

    //La validation pour l'erreur
    $('#6_1, #6_2, #6_3, #6_4, #6_5').click(function () {
        reponse6 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#6_1').click(function () {
        $('.appreciationValide6').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation6').text('');
    })

    $('#6_2').click(function () {
        $('.appreciationValide6').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation6').text('');
    })

    $('#6_3').click(function () {
        $('.appreciationValide6').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation6').text('');
    })

    $('#6_4').click(function () {
        $('.appreciationValide6').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation6').text('');
    })

    $('#6_5').click(function () {
        $('.appreciationValide6').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation6').text('');
    })


    //Quand la souris entre, un aperçu est disponible
    $('#7_1').mouseenter(function () {
        $('.appreciation7').text('Très insatisfait').css({
            color: 'black'
        });
    })

    $('#7_2').mouseenter(function () {
        $('.appreciation7').text('Insatisfait').css({
            color: 'black'
        });
    })

    $('#7_3').mouseenter(function () {
        $('.appreciation7').text('Moyen').css({
            color: 'black'
        });
    })

    $('#7_4').mouseenter(function () {
        $('.appreciation7').text('Satisfait').css({
            color: 'black'
        });
    })

    $('#7_5').mouseenter(function () {
        $('.appreciation7').text('Très satisfait').css({
            color: 'black'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#7_1, #7_2, #7_3, #7_4, #7_5').mouseleave(function () {
        $('.appreciation7').text('');
    })

    //La validation pour l'erreur
    $('#7_1, #7_2, #7_3, #7_4, #7_5').click(function () {
        reponse7 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#7_1').click(function () {
        $('.appreciationValide7').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation7').text('');
    })

    $('#7_2').click(function () {
        $('.appreciationValide7').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px'
        });
        $('.appreciation7').text('');
    })

    $('#7_3').click(function () {
        $('.appreciationValide7').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px'
        });
        $('.appreciation7').text('');
    })

    $('#7_4').click(function () {
        $('.appreciationValide7').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation7').text('');
    })

    $('#7_5').click(function () {
        $('.appreciationValide7').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px'
        });
        $('.appreciation7').text('');
    })
    
    
    var tag = $('#tag');

    tag.keydown(function () {
        $('.nombreCharactere').text('Nombre de charactères restant : ' + (200 - $(this).val().length)).css({
            font: 'calibri',
            fontSize: '14px'
        });
    })


    //Apparition de l'aide quand la souris entre
    $('.aide1').click(function () {
        $('.texteAide1').toggle();
    })

    $('.aide2').click(function () {
        $('.texteAide2').toggle();
    })

    $('.aide3').click(function () {
        $('.texteAide3').toggle();
    })

    $('.aide4').click(function () {
        $('.texteAide4').toggle();
    })


    $('.suivant5').click(function (event) {
        if (reponse1 == false || reponse2 == false || reponse3 == false || reponse4 == false || reponse5 == false || reponse6 == false) {
            event.preventDefault();
        }
        if (reponse1 == false) {
            $('.appreciation1').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse2 == false) {
            $('.appreciation2').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse3 == false) {
            $('.appreciation3').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse4 == false) {
            $('.appreciation4').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse5 == false) {
            $('.appreciation5').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse6 == false) {
            $('.appreciation6').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        
        if(reponse1 == true && reponse2 == true && reponse3 == true && reponse4 == true && reponse5 == true && reponse6 == true){
            $('.fondBlanc2, .fondBlanc3').hide();
            $('.confirmation').show();
            
            $('.pseudo').keyup(function(){
                var value = $(this).val();
                $('.valuePseudo').text(value);
            })
            .keyup();
            
            $('.titreComm').keyup(function(){
                var valueTitreComm = $(this).val();
                $('.valueTitreComm').text(valueTitreComm);
            })            
            .keyup();
            
            $('#tag').keyup(function(){
                var valueComm = $(this).val();
                $('.valueComm').text(valueComm);
            })            
            .keyup();
            
        }
    });


    /*$('.suivant3').click(function () {
        window.open('/', '_self', false);
    })*/

    $('.modifier').click(function () {
        $('.confirmation').hide();
        $('.fondBlanc2, .fondBlanc3').show();
    })

});