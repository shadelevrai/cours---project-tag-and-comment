$(function () {
    $.ajax({
        type: 'GET',
        url: '/api/affiche0',
        success: function (data) {
            $('.liste').html('<h2>Nouveaux avis</h2>');
            for (var i = 0; i < data.length; i++) {
                $('.listeAvis').append('<p id="titreComm">' + data[i].TitreCommentaire + '</p>');
                $('.listeAvis').append('<p>' + data[i].Pseudo + '</p>');
                $('.listeAvis').append('<p>' + data[i].Date + '</p>');
                $('.listeAvis').append('<p>' + data[i].Commentaire + '</p>');
                $('.listeAvis').append('<a href="/validate/'+ data[i]._id +'"><img src="img/validate.png" alt="ok" height="50" width="50"/></a>');
                $('.listeAvis').append('<a href="/delete/'+ data[i]._id +'"><img src="img/supprime.png" alt="ok" height="50" width="50"/></a>');
                $('.listeAvis').append('<br></br>');
                $('.listeAvis').append('<p>-------------------------------------------------------</p>');
            }
        }
    });
});