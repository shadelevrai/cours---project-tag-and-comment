//------------------------------ La moyenne générale de la formation-------------------------------------------//

$(function () {
    $.ajax({
        type: 'GET',
        url: '/api/moyenne',
        success: function (data) {

            if (data[0].moyenne < 0.25) {
                $('.moyenne').append('<img src="img/0star.png">');
            }
            if (data[0].moyenne > 0.25 && data[0].moyenne < 0.75 || data[0].moyenne == 0.5) {
                $('.moyenne').append('<img src="img/05star.png">');
            }
            if (data[0].moyenne > 0.75 && data[0].moyenne < 1.25 || data[0].moyenne == 1) {
                $('.moyenne').append('<img src="img/10star.png">');
            }
            if (data[0].moyenne > 1.25 && data[0].moyenne < 1.75 || data[0].moyenne == 1.5) {
                $('.moyenne').append('<img src="img/15star.png">');
            }
            if (data[0].moyenne > 1.75 && data[0].moyenne < 2.25 || data[0].moyenne == 2) {
                $('.moyenne').append('<img src="img/20star.png">');
            }
            if (data[0].moyenne > 2.25 && data[0].moyenne < 2.75 || data[0].moyenne == 2.5) {
                $('.moyenne').append('<img src="img/25star.png">');
            }
            if (data[0].moyenne > 2.75 && data[0].moyenne < 3.25 || data[0].moyenne == 3) {
                $('.moyenne').append('<img src="img/30star.png">');
            }
            if (data[0].moyenne > 3.25 && data[0].moyenne < 3.75 || data[0].moyenne == 3.5) {
                $('.moyenne').append('<img src="img/35star.png">');
            }
            if (data[0].moyenne > 3.75 && data[0].moyenne < 4.25 || data[0].moyenne == 4) {
                $('.moyenne').append('<img src="img/40star.png">');
            }
            if (data[0].moyenne > 4.25 && data[0].moyenne < 4.75 || data[0].moyenne == 4.5) {
                $('.moyenne').append('<img src="img/45star.png">');
            }
            if (data[0].moyenne > 4.75) {
                $('.moyenne').append('<img src="img/50star.png">');
            }



        }
    });

    //-----------------------------------------La note générale de chaque personne---------------------------------//    

    $.ajax({
        type: 'GET',
        url: '/api/affiche1',
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                $('.avis').append('<div class="cadre' + [i] + '"></div>');
                $('.cadre' + [i] + '').css({
                    width: 'auto',
                    height: '400px',
                    backgroundColor: 'white',
                    border: '1px solid black'
                })
                $('.cadre' + [i] + '').append('<p class="pseudo">' + data[i].Pseudo + '</p>');
                $('.cadre' + [i] + '').append('<p class="question">Lui poser une question</p>');

                //La note générale de la personne
                if (data[i].AvisGeneral < 0.25) {
                    $('.cadre' + [i] + '').append('<img src="img/0star.png">');
                }
                if (data[i].AvisGeneral > 0.25 && data[i].AvisGeneral < 0.75 || data[i].AvisGeneral == 0.5) {
                    $('.cadre' + [i] + '').append('<img src="img/05star.png" class="star">');
                }
                if (data[i].AvisGeneral > 0.75 && data[i].AvisGeneral < 1.25 || data[i].AvisGeneral == 1) {
                    $('.cadre' + [i] + '').append('<img src="img/10star.png" class="star">');
                }
                if (data[i].AvisGeneral > 1.25 && data[i].AvisGeneral < 1.75 || data[i].AvisGeneral == 1.5) {
                    $('.cadre' + [i] + '').append('<img src="img/15star.png" class="star">');
                }
                if (data[i].AvisGeneral > 1.75 && data[i].AvisGeneral < 2.25 || data[i].AvisGeneral == 2) {
                    $('.cadre' + [i] + '').append('<img src="img/20star.png" class="star">');
                }
                if (data[i].AvisGeneral > 2.25 && data[i].AvisGeneral < 2.75 || data[i].AvisGeneral == 2.5) {
                    $('.cadre' + [i] + '').append('<img src="img/25star.png" class="star">');
                }
                if (data[i].AvisGeneral > 2.75 && data[i].AvisGeneral < 3.25 || data[i].AvisGeneral == 3) {
                    $('.cadre' + [i] + '').append('<img src="img/30star.png" class="star">');
                }
                if (data[i].AvisGeneral > 3.25 && data[i].AvisGeneral < 3.75 || data[i].AvisGeneral == 3.5) {
                    $('.cadre' + [i] + '').append('<img src="img/35star.png" class="star">');
                }
                if (data[i].AvisGeneral > 3.75 && data[i].AvisGeneral < 4.25 || data[i].AvisGeneral == 4) {
                    $('.cadre' + [i] + '').append('<img src="img/40star.png" class="star">');
                }
                if (data[i].AvisGeneral > 4.25 && data[i].AvisGeneral < 4.75 || data[i].AvisGeneral == 4.5) {
                    $('.cadre' + [i] + '').append('<img src="img/45star.png" class="star">');
                }
                if (data[i].AvisGeneral > 4.75) {
                    $('.cadre' + [i] + '').append('<img src="img/50star.png" class="star">');
                }

                //Cadre du commentaire
                $('.cadre' + [i] + '').append('<div class="petitCadre' + [i] + '"></div>');

                $('.petitCadre' + [i] + '').css({
                    width: '400px',
                    height: '200px',
                    backgroundColor: '#ebffff',
                    position: 'absolute',
                    marginLeft: '40%',
                    marginTop: '65px'
                })

                $('.petitCadre' + [i] + '').append('<p class="titreCommentaire">' + data[i].TitreCommentaire + '</p>');

                $('.petitCadre' + [i] + '').append('<p class="commentaire">' + data[i].Commentaire + '</p>');


                //Note formation et établissement
                $('.cadre' + [i] + '').append('<p class="formation">Formation</p>');
                $('.cadre' + [i] + '').append('<p class="etablissement">Établissement</p>');

                moyenneFormation = (data[i].Avis03 + data[i].Avis05 + data[i].Avis06) / 3;
                moyenneEtablissement = (data[i].Avis01 + data[i].Avis02 + data[i].Avis04) / 3;

                if (moyenneFormation < 0.25) {
                    $('.cadre' + [i] + '').append('<img src="img/0star.png">');
                }
                if (moyenneFormation > 0.25 && moyenneFormation < 0.75 || moyenneFormation == 0.5) {
                    $('.cadre' + [i] + '').append('<img src="img/05star.png" class="starFormation">');
                }
                if (moyenneFormation > 0.75 && moyenneFormation < 1.25 || moyenneFormation == 1) {
                    $('.cadre' + [i] + '').append('<img src="img/10star.png" class="starFormation">');
                }
                if (moyenneFormation > 1.25 && moyenneFormation < 1.75 || moyenneFormation == 1.5) {
                    $('.cadre' + [i] + '').append('<img src="img/15star.png" class="starFormation">');
                }
                if (moyenneFormation > 1.75 && moyenneFormation < 2.25 || moyenneFormation == 2) {
                    $('.cadre' + [i] + '').append('<img src="img/20star.png" class="starFormation">');
                }
                if (moyenneFormation > 2.25 && moyenneFormation < 2.75 || moyenneFormation == 2.5) {
                    $('.cadre' + [i] + '').append('<img src="img/25star.png" class="starFormation">');
                }
                if (moyenneFormation > 2.75 && moyenneFormation < 3.25 || moyenneFormation == 3) {
                    $('.cadre' + [i] + '').append('<img src="img/30star.png" class="starFormation">');
                }
                if (moyenneFormation > 3.25 && moyenneFormation < 3.75 || moyenneFormation == 3.5) {
                    $('.cadre' + [i] + '').append('<img src="img/35star.png" class="starFormation">');
                }
                if (moyenneFormation > 3.75 && moyenneFormation < 4.25 || moyenneFormation == 4) {
                    $('.cadre' + [i] + '').append('<img src="img/40star.png" class="starFormation">');
                }
                if (moyenneFormation > 4.25 && moyenneFormation < 4.75 || moyenneFormation == 4.5) {
                    $('.cadre' + [i] + '').append('<img src="img/45star.png" class="starFormation">');
                }
                if (moyenneFormation > 4.75) {
                    $('.cadre' + [i] + '').append('<img src="img/50star.png" class="starFormation">');
                }



                if (moyenneEtablissement < 0.25) {
                    $('.cadre' + [i] + '').append('<img src="img/0star.png"class="starEtablissement">');
                }
                if (moyenneEtablissement > 0.25 && moyenneEtablissement < 0.75 || moyenneEtablissement == 0.5) {
                    $('.cadre' + [i] + '').append('<img src="img/05star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 0.75 && moyenneEtablissement < 1.25 || moyenneEtablissement == 1) {
                    $('.cadre' + [i] + '').append('<img src="img/10star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 1.25 && moyenneEtablissement < 1.75 || moyenneEtablissement == 1.5) {
                    $('.cadre' + [i] + '').append('<img src="img/15star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 1.75 && moyenneEtablissement < 2.25 || moyenneEtablissement == 2) {
                    $('.cadre' + [i] + '').append('<img src="img/20star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 2.25 && moyenneEtablissement < 2.75 || moyenneEtablissement == 2.5) {
                    $('.cadre' + [i] + '').append('<img src="img/25star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 2.75 && moyenneEtablissement < 3.25 || moyenneEtablissement == 3) {
                    $('.cadre' + [i] + '').append('<img src="img/30star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 3.25 && moyenneEtablissement < 3.75 || moyenneEtablissement == 3.5) {
                    $('.cadre' + [i] + '').append('<img src="img/35star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 3.75 && moyenneEtablissement < 4.25 || moyenneEtablissement == 4) {
                    $('.cadre' + [i] + '').append('<img src="img/40star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 4.25 && moyenneEtablissement < 4.75 || moyenneEtablissement == 4.5) {
                    $('.cadre' + [i] + '').append('<img src="img/45star.png" class="starEtablissement">');
                }
                if (moyenneEtablissement > 4.75) {
                    $('.cadre' + [i] + '').append('<img src="img/50star.png" class="starEtablissement">');
                }


                $('.avis').append('<br></br>');
                //$('.avis').append('<p>Lalalaii</p>');
                //$('.avis').append('<p id="titreComm">' + data[i].TitreCommentaire + '</p>');
                //$('.listeAvis').append('<p>' + data[i].Date + '</p>');
                //$('.listeAvis').append('<p>' + data[i].Commentaire + '</p>');
            }
        }
    })

});