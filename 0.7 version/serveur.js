var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://127.0.0.1:27017/jeux';
var device = require('express-device');
var nodemailer = require('nodemailer');

var myDate = new Date;

app.use(device.capture());
app.use(express.static(__dirname + '/fichiers'));
app.use(bodyParser.urlencoded({
    extended: false
}));

app.set('view engine', 'jade');
app.set('views', './fichiers');

//------------------Mot refusés-------------------------------------//

var insulte = /(salope|pute|merde|fuck|abruti|branler|chier|chieur|con | con.| con,|connar|connard|conne|couillon|crétin|cretin| cul |enculé|encule|enflure|enfoiré|enfoire|foutre|garce|gouine|pd|grognasse|merdeux|niquer|nique|pédé|pétasse|petasse|pede|péteux|peteux|pouffiasse|poufiasse|putain|put1|salaud|salop|salope|saloperie|saloperi|salopard|salopar|traînée|trainée|trainé|fdp|étron|fiote| bite |bougnoul|bougnoule|branleur|couille|couilles|lavette|lopette|négro|nègre|negre|fucker|enculer|salopes|putes|merdes|fucks|abrutis|branlers|chiers|chieurs| cons |connars|connards|connes|couillons|crétins|cretins|culs|enculés|encules|enflures|enfoirés|enfoires|foutres|garces|gouines|pds|grognasses|merdeux|niquer|niques|pédés|pétasses|petasses|pedes|péteux|peteux|pouffiasses|poufiasses|putain|put1|salauds|salop|salopes|saloperies|saloperi|salopards|salopars|traînées|trainées|trainés|fdp|étrons|fiotes|bites|bougnouls|bougnoules|batard|branleurs|couilles|couilles|lavettes|lopettes|négros|nègres|negres|fucker|enculer|http:|www|.com|.fr|@)/i;

//------------------Page enquete-----------------------------//

app.get('/hello', function (req, res) {
    res.send("Hi to " + req.device.type.toUpperCase() + " User");
});

app.get('/', function (req, res) {
    name = req.query["name"];
    formationName = req.query["formationname"];
    formationPlace = req.query["formationplace"];
    beginDate = req.query["begindate"];
    endingDate = req.query["endingdate"];

    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            if (name == null) {
                res.send('Accès refusé');
            } else {
                
                //console.log('Connection established to', url);

                db.collection('temp').find({
                    name: name
                }).count(function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        if (result > 0) {
                            console.log(name + ' a tenté de redonné son avis');
                            res.render('accesDenied');
                        } else {
                            if (req.device.type.toUpperCase() == "DESKTOP") {
                                res.render('index', {
                                    name: name,
                                    formationName: formationName,
                                    formationPlace: formationPlace,
                                    beginDate: beginDate,
                                    endingDate: endingDate
                                });
                                console.log(name + ' s\'est connecté(e)');
                            } else {
                                res.render('./Mobile/indexMobile1', {
                                    name: name,
                                    formationName: formationName,
                                    formationPlace: formationPlace,
                                    beginDate: beginDate,
                                    endingDate: endingDate
                                });
                                console.log(name + ' s\'est connecté(e)');
                            }
                        }
                    }
                });
            }
        }
    });
})

app.get('/index2', function (req, res) {
    if (req.device.type.toUpperCase() == "DESKTOP") {
        res.render('index2'/*, {
            formationPlace : formationPlace,
            formationName : formationName
        }*/);
    } else {
        res.render('./Mobile/indexMobile2');
    }
})

app.get('/index3' ,function (req, res) {
    if(req.device.type.toUpperCase() == "DESKTOP") {
        //res.render('index3');
        console.log('ok');
    } else {
        res.render('./Mobile/indexMobile3')
    }
})

app.get('/admin', function (req, res) {
    res.render('admin');
})

app.get('/validation', function (req, res) {
    res.render('validation');
})

app.get('/supprime', function (req, res) {
    res.render('supprime');
})

app.get('/contact', function (req, res) {
    res.render('contact');
})

app.get('/apiLBF', function (req, res) {
        res.render('apiLBF');
    })
    //----------------------Envois d'infos--------------------------//
app.post('/envoiInfo', function (req, res) {

    var avis1 = req.body.avis1;
    var avis2 = req.body.avis2;
    var avis3 = req.body.avis3;
    var avis4 = req.body.avis4;
    var avis5 = req.body.avis5;
    var avis6 = req.body.avis6;
    var avisGeneral = req.body.avisGeneral;
    var pseudo = req.body.pseudo;
    var titreComm = req.body.titreCommentaire;
    var comm = req.body.commentaire;
    var accord = req.body.accord;
    var validation = 0;

    //3 : validé automatiquement
    //2 : refusé
    //1 : validé
    //0 : en attente

    if (titreComm == '' && comm == '' && titreComm == '') {
        validation = 3;
    }
    //étape 1 de la modération
    var o = pseudo.search(insulte);
    var p = titreComm.search(insulte);
    var n = comm.search(insulte);
    if (n >= 0 || o >= 0 || p >= 0) {
        console.log('Une personne a mis une insulte');
        res.render('refus');
        console.log(o);
        console.log(p);
        console.log(n);
    } else {
        console.log('Première étape de modération : OK');
        console.log(o);
        console.log(p);
        console.log(n);





        if (comm == '') {
            comm = false;
        }

        console.log('--------------------');
        console.log('nom : ' + name);
        console.log('nom de la formation : ' + formationName);
        console.log('Lieu de la formation : ' + formationPlace);
        console.log('Date de début : ' + beginDate);
        console.log('Premier avis : ' + avis1);
        console.log('Second avis : ' + avis2);
        console.log('Troisième avis : ' + avis3);
        console.log('Quatrième avis : ' + avis4);
        console.log('Cinquième avis : ' + avis5);
        console.log('Sixième avis : ' + avis6);
        console.log('Avis Général : ' + avisGeneral);
        console.log('Pseudo : ' + pseudo);
        console.log('Titre commentaire : ' + titreComm);
        console.log('Commentaire : ' + comm);
        console.log('Accord de contacte : ' + accord);
        console.log('date : ' + myDate);
        console.log('Validation : ' + validation);
        console.log('--------------------');

        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {


                db.collection('temp').find({
                    name: name
                }).count(function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        if (result > 0) {
                            console.log('Une personne qui a déjà donné son avis a tenté de le refaire');
                            res.render('accesDenied');
                        } else {
                            db.collection('temp').insert({
                                name: name,
                                formationName: formationName,
                                formationPlace: formationPlace,
                                beginDate: beginDate,
                                Pseudo: pseudo,
                                TitreCommentaire: titreComm,
                                Commentaire: comm,
                                Avis01: parseFloat(avis1),
                                Avis02: parseFloat(avis2),
                                Avis03: parseFloat(avis3),
                                Avis04: parseFloat(avis4),
                                Avis05: parseFloat(avis5),
                                Avis06: parseFloat(avis6),
                                AvisGeneral: parseFloat(avisGeneral),
                                Date: myDate.toLocaleString(),
                                Validation: validation,
                                Accord : accord
                            });
                            if (req.device.type.toUpperCase() == "DESKTOP") {
                                res.render('index3');
                            } else {
                                res.render('./Mobile/indexMobile3');
                            }
                        }
                    }
                });
            }
        });


        var avis1Phrase = false;
        var avis2Phrase = false;
        var avis3Phrase = false;
        var avis4Phrase = false;
        var avis5Phrase = false;
        var avis6Phrase = false;
        var avisGeneralPhrase = false;

        if (avis1 == 1) {
            avis1Phrase = 'Très insatisfait';
        }
        if (avis1 == 2) {
            avis1Phrase = 'Insatisfait';
        }
        if (avis1 == 3) {
            avis1Phrase = 'Moyennement satisfait';
        }
        if (avis1 == 4) {
            avis1Phrase = 'Satisfait';
        }
        if (avis1 == 5) {
            avis1Phrase = 'Très satisfait';
        }

        if (avis2 == 1) {
            avis2Phrase = 'Très insatisfait';
        }
        if (avis2 == 2) {
            avis2Phrase = 'Insatisfait';
        }
        if (avis2 == 3) {
            avis2Phrase = 'Moyennement satisfait';
        }
        if (avis2 == 4) {
            avis2Phrase = 'Satisfait';
        }
        if (avis2 == 5) {
            avis2Phrase = 'Très satisfait';
        }

        if (avis3 == 1) {
            avis3Phrase = 'Très insatisfait';
        }
        if (avis3 == 2) {
            avis3Phrase = 'Insatisfait';
        }
        if (avis3 == 3) {
            avis3Phrase = 'Moyennement satisfait';
        }
        if (avis3 == 4) {
            avis3Phrase = 'Satisfait';
        }
        if (avis3 == 5) {
            avis3Phrase = 'Très satisfait';
        }

        if (avis4 == 1) {
            avis4Phrase = 'Très insatisfait';
        }
        if (avis4 == 2) {
            avis4Phrase = 'Insatisfait';
        }
        if (avis4 == 3) {
            avis4Phrase = 'Moyennement satisfait';
        }
        if (avis4 == 4) {
            avis4Phrase = 'Satisfait';
        }
        if (avis4 == 5) {
            avis4Phrase = 'Très satisfait';
        }

        if (avis5 == 1) {
            avis5Phrase = 'Très insatisfait';
        }
        if (avis5 == 2) {
            avis5Phrase = 'Insatisfait';
        }
        if (avis5 == 3) {
            avis5Phrase = 'Moyennement satisfait';
        }
        if (avis5 == 4) {
            avis5Phrase = 'Satisfait';
        }
        if (avis5 == 5) {
            avis5Phrase = 'Très satisfait';
        }

        if (avis6 == 1) {
            avis6Phrase = 'Très insatisfait';
        }
        if (avis6 == 2) {
            avis6Phrase = 'Insatisfait';
        }
        if (avis6 == 3) {
            avis6Phrase = 'Moyennement satisfait';
        }
        if (avis6 == 4) {
            avis6Phrase = 'Satisfait';
        }
        if (avis6 == 5) {
            avis6Phrase = 'Très satisfait';
        }

        if (avisGeneral == 1) {
            avisGeneralPhrase = 'Très insatisfait';
        }
        if (avisGeneral == 2) {
            avisGeneralPhrase = 'Insatisfait';
        }
        if (avisGeneral == 3) {
            avisGeneralPhrase = 'Moyennement satisfait';
        }
        if (avisGeneral == 4) {
            avisGeneralPhrase = 'Satisfait';
        }
        if (avisGeneral == 5) {
            avisGeneralPhrase = 'Très satisfait';
        }
        if (comm == false) {
            comm = '';
        }

    }
})



//---------------------------------test--------------------------------//



//----------------------------Get et post-------------------------------------//

//Lire nouveaux commentaires
app.get('/api/affiche0', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 0
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//Lire commentaires validés
app.get('/api/affiche1', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 1
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//Lire commentaires supprimés
app.get('/api/affiche2', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').find({
                Validation: 2
            }).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });

        }
    });
})

//Supprimé
app.get('/delete/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/admin');
        }
    });
});

app.get('/delete2/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/validation');
        }
    });
});

app.get('/delete3/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 2
                }
            });
            //db.collection('temp').deleteOne({"_id": ObjectId(req.params.id)});
            console.log('Le commentaire a été supprimé');
            res.redirect('/supprime');
        }
    });
});

//Validé
app.get('/validate/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/admin');
        }
    });
});

app.get('/validate2/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/validation');
        }
    });
});

app.get('/validate3/:id', function (req, res) {
    MongoClient.connect(url, function (err, db, result) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);
            db.collection('temp').update({
                "_id": ObjectId(req.params.id)
            }, {
                $set: {
                    "Validation": 1
                }
            });
            console.log('Le commentaire a été validé');
            res.redirect('/supprime');
        }
    });
});

//envoi la moyenne
app.get('/api/moyenne', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            //console.log('Connection established to', url);

            db.collection('temp').aggregate([
                {
                    $match: {
                        Validation: 1
                    }
                },
                {
                    $group: {
                        _id: null,
                        moyenne: {
                            $avg: '$AvisGeneral'
                        }
                    }
                }
            ]).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})

//envoi la moyenne des 3 première questions sur la formatio
/*app.get('/api/moyenneFormation', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').aggregate([
                {
                    $match: {
                        Validation : 1
                    }
                },
                {
                    $group: {
                        _id: null,
                        moyenne: {
                            $avg: '$Avis01', '$Avis02', '$Avis03'
                        }
                    }
                }
            ]).toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})*/

var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});