$(function () {

    /********************************page 01*********************************/

    $('.suivant').click(function () {
        window.open('/index2', '_self', false);
    })

    /********************************page 02**********************************/

    $('.fondBlanc2').hide();
    $('.fondBlanc3').hide();

    var reponse1 = false;
    var reponse2 = false;
    var reponse3 = false;
    var reponse4 = false;
    var reponse5 = false;
    var reponse6 = false;
    var reponse7 = false;

    //Quand la souris entre, un aperçu est disponible
    $('#1_1').mouseenter(function () {
        $('.appreciation1').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#1_2').mouseenter(function () {
        $('.appreciation1').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#1_3').mouseenter(function () {
        $('.appreciation1').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#1_4').mouseenter(function () {
        $('.appreciation1').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#1_5').mouseenter(function () {
        $('.appreciation1').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })


    //Quand la souris sort, l'aperçu disparait
    $('#1_1, #1_2, #1_3, #1_4, #1_5').mouseleave(function () {
        $('.appreciation1').text('');
    })

    //La validation pour l'erreur
    $('#1_1, #1_2, #1_3, #1_4, #1_5').click(function () {
        reponse1 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#1_1').click(function () {
        $('.appreciationValide1').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation1').text('');
        appreciation1Img = 'Très insatisfait';
    })

    $('#1_2').click(function () {
        $('.appreciationValide1').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation1').text('');
        appreciation1Img = 'Insatisfait';
    })

    $('#1_3').click(function () {
        $('.appreciationValide1').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation1').text('');
        appreciation1Img = 'Moyen';
    })

    $('#1_4').click(function () {
        $('.appreciationValide1').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation1').text('');
        appreciation1Img = 'Satisfait';
    })

    $('#1_5').click(function () {
        $('.appreciationValide1').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation1').text('');
        appreciation1Img = 'Très satisfait';
    })



    //Quand la souris entre, un aperçu est disponible
    $('#2_1').mouseenter(function () {
        $('.appreciation2').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#2_2').mouseenter(function () {
        $('.appreciation2').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#2_3').mouseenter(function () {
        $('.appreciation2').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#2_4').mouseenter(function () {
        $('.appreciation2').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#2_5').mouseenter(function () {
        $('.appreciation2').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#2_1, #2_2, #2_3, #2_4, #2_5').mouseleave(function () {
        $('.appreciation2').text('');
    })

    //La validation pour l'erreur
    $('#2_1, #2_2, #2_3, #2_4, #2_5').click(function () {
        reponse2 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#2_1').click(function () {
        $('.appreciationValide2').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation2').text('');
        appreciation2Img = 'Très insatisfait';
    })

    $('#2_2').click(function () {
        $('.appreciationValide2').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation2').text('');
        appreciation2Img = 'Insatisfait';
    })

    $('#2_3').click(function () {
        $('.appreciationValide2').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation2').text('');
        appreciation2Img = 'Moyen';
    })

    $('#2_4').click(function () {
        $('.appreciationValide2').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation2').text('');
        appreciation2Img = 'Satisfait';
    })

    $('#2_5').click(function () {
        $('.appreciationValide2').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation2').text('');
        appreciation2Img = 'Très satisfait';
    })



    //Quand la souris entre, un aperçu est disponible
    $('#3_1').mouseenter(function () {
        $('.appreciation3').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#3_2').mouseenter(function () {
        $('.appreciation3').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#3_3').mouseenter(function () {
        $('.appreciation3').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#3_4').mouseenter(function () {
        $('.appreciation3').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#3_5').mouseenter(function () {
        $('.appreciation3').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#3_1, #3_2, #3_3, #3_4, #3_5').mouseleave(function () {
        $('.appreciation3').text('');
    })

    //La validation pour l'erreur
    $('#3_1, #3_2, #3_3, #3_4, #3_5').click(function () {
        reponse3 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#3_1').click(function () {
        $('.appreciationValide3').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation3').text('');
        appreciation3Img = 'Très insatisfait';
    })

    $('#3_2').click(function () {
        $('.appreciationValide3').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation3').text('');
        appreciation3Img = 'Insatisfait';
    })

    $('#3_3').click(function () {
        $('.appreciationValide3').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation3').text('');
        appreciation3Img = 'Moyen';
    })

    $('#3_4').click(function () {
        $('.appreciationValide3').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation3').text('');
        appreciation3Img = 'Satisfait';
    })

    $('#3_5').click(function () {
        $('.appreciationValide3').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation3').text('');
        appreciation3Img = 'Très satisfait';
    })


    //Quand la souris entre, un aperçu est disponible
    $('#4_1').mouseenter(function () {
        $('.appreciation4').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#4_2').mouseenter(function () {
        $('.appreciation4').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#4_3').mouseenter(function () {
        $('.appreciation4').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#4_4').mouseenter(function () {
        $('.appreciation4').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#4_5').mouseenter(function () {
        $('.appreciation4').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#4_1, #4_2, #4_3, #4_4, #4_5').mouseleave(function () {
        $('.appreciation4').text('');
    })

    //La validation pour l'erreur
    $('#4_1, #4_2, #4_3, #4_4, #4_5').click(function () {
        reponse4 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#4_1').click(function () {
        $('.appreciationValide4').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation4').text('');
        appreciation4Img = 'Très insatisfait';
    })

    $('#4_2').click(function () {
        $('.appreciationValide4').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation4').text('');
        appreciation4Img = 'Insatisfait';
    })

    $('#4_3').click(function () {
        $('.appreciationValide4').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation4').text('');
        appreciation4Img = 'Moyen';
    })

    $('#4_4').click(function () {
        $('.appreciationValide4').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation4').text('');
        appreciation4Img = 'Satisfait';
    })

    $('#4_5').click(function () {
        $('.appreciationValide4').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation4').text('');
        appreciation4Img = 'Très satisfait';
    })


    //Quand la souris entre, un aperçu est disponible
    $('#5_1').mouseenter(function () {
        $('.appreciation5').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#5_2').mouseenter(function () {
        $('.appreciation5').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#5_3').mouseenter(function () {
        $('.appreciation5').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#5_4').mouseenter(function () {
        $('.appreciation5').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#5_5').mouseenter(function () {
        $('.appreciation5').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#5_1, #5_2, #5_3, #5_4, #5_5').mouseleave(function () {
        $('.appreciation5').text('');
    })

    //La validation pour l'erreur
    $('#5_1, #5_2, #5_3, #5_4, #5_5').click(function () {
        reponse5 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#5_1').click(function () {
        $('.appreciationValide5').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation5').text('');
        appreciation5Img = 'Très insatisfait';
    })

    $('#5_2').click(function () {
        $('.appreciationValide5').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation5').text('');
        appreciation5Img = 'Insatisfait';
    })

    $('#5_3').click(function () {
        $('.appreciationValide5').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation5').text('');
        appreciation5Img = 'Moyen';
    })

    $('#5_4').click(function () {
        $('.appreciationValide5').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation5').text('');
        appreciation5Img = 'Satisfait';
    })

    $('#5_5').click(function () {
        $('.appreciationValide5').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation5').text('');
        appreciation5Img = 'Très satisfait';
    })


    //Quand la souris entre, un aperçu est disponible
    $('#6_1').mouseenter(function () {
        $('.appreciation6').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#6_2').mouseenter(function () {
        $('.appreciation6').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#6_3').mouseenter(function () {
        $('.appreciation6').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#6_4').mouseenter(function () {
        $('.appreciation6').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#6_5').mouseenter(function () {
        $('.appreciation6').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#6_1, #6_2, #6_3, #6_4, #6_5').mouseleave(function () {
        $('.appreciation6').text('');
    })

    //La validation pour l'erreur
    $('#6_1, #6_2, #6_3, #6_4, #6_5').click(function () {
        reponse6 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#6_1').click(function () {
        $('.appreciationValide6').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation6').text('');
        appreciation6Img = 'Très insatisfait';
    })

    $('#6_2').click(function () {
        $('.appreciationValide6').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation6').text('');
        appreciation6Img = 'Insatisfait';
    })

    $('#6_3').click(function () {
        $('.appreciationValide6').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation6').text('');
        appreciation6Img = 'Moyen';
    })

    $('#6_4').click(function () {
        $('.appreciationValide6').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation6').text('');
        appreciation6Img = 'Satisfait';
    })

    $('#6_5').click(function () {
        $('.appreciationValide6').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation6').text('');
        appreciation6Img = 'Très satisfait';
    })


    //Quand la souris entre, un aperçu est disponible
    $('#7_1').mouseenter(function () {
        $('.appreciation7').text('Très insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#7_2').mouseenter(function () {
        $('.appreciation7').text('Insatisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#7_3').mouseenter(function () {
        $('.appreciation7').text('Moyen').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#7_4').mouseenter(function () {
        $('.appreciation7').text('Satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    $('#7_5').mouseenter(function () {
        $('.appreciation7').text('Très satisfait').css({
            color: 'black',
            fontSize : '12px'
        });
    })

    //Quand la souris sort, l'aperçu disparait
    $('#7_1, #7_2, #7_3, #7_4, #7_5').mouseleave(function () {
        $('.appreciation7').text('');
    })

    //La validation pour l'erreur
    $('#7_1, #7_2, #7_3, #7_4, #7_5').click(function () {
        reponse7 = true;
    })

    //Quand on clique, l'aperçu disprait et l'appreciation valide apparait en vert
    $('#7_1').click(function () {
        $('.appreciationValide7').text('Très insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation7').text('');
        appreciation7Img = 'Très insatisfait';
    })

    $('#7_2').click(function () {
        $('.appreciationValide7').text('Insatisfait').css({
            color: 'red',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation7').text('');
        appreciation7Img = 'Insatisfait';
    })

    $('#7_3').click(function () {
        $('.appreciationValide7').text('Moyen').css({
            color: 'orange',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation7').text('');
        appreciation7Img = 'Moyen';
    })

    $('#7_4').click(function () {
        $('.appreciationValide7').text('Satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation7').text('');
        appreciation7Img = 'Satisfait';
    })

    $('#7_5').click(function () {
        $('.appreciationValide7').text('Très satisfait').css({
            color: 'green',
            marginTop: '-10px',
            fontSize : '12px'
        });
        $('.appreciation7').text('');
        appreciation7Img = 'Très satisfait';
    })



    $('.retour').click(function () {
        $('.fondBlanc2').hide();
        $('.fondBlanc1').show();
    });

    var tag = $('#tag');

    tag.keydown(function () {
        $('.nombreCharactere').text('Nombre de charactères restant : ' + (200 - $(this).val().length)).css({
            font: 'calibri',
            fontSize: '14px'
        });
    })


    //Apparition de l'aide quand la souris entre
    $('.aide1').click(function () {
        $('.texteAide1').toggle();
    })

    $('.aide2').click(function () {
        $('.texteAide2').toggle();
    })

    $('.aide3').click(function () {
        $('.texteAide3').toggle();
    })

    $('.aide4').click(function () {
        $('.texteAide4').toggle();
    })


    $('.etapeSuivante').click(function (event) {
        if (reponse1 == false || reponse2 == false || reponse3 == false || reponse4 == false || reponse5 == false || reponse6 == false || reponse7 == false) {
            event.preventDefault();
        }
        if (reponse1 == false) {
            $('.appreciation1').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse2 == false) {
            $('.appreciation2').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse3 == false) {
            $('.appreciation3').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse4 == false) {
            $('.appreciation4').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse5 == false) {
            $('.appreciation5').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        if (reponse6 == false) {
            $('.appreciation6').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }
        
        if (reponse7 == false) {
            $('.appreciation7').text('Oups, vous avez oublié cet avis').css({
                color: 'palevioletred'
            });
        }

        if (reponse1 == true && reponse2 == true && reponse3 == true && reponse4 == true && reponse5 == true && reponse6 == true && reponse7 == true) {
            $('.fondBlanc1').hide();
            $('.fondBlanc2').show();

        }
    });


    $('.suivant5').click(function () {
        
        //Les étoiles qui apparaissent au récapitulatif
        if(appreciation1Img == "Très insatisfait") {
            $('.star1').attr('src','img/10star.png');
        }
        if(appreciation1Img == "Insatisfait") {
            $('.star1').attr('src','img/20star.png');
        }
        if(appreciation1Img == "Moyen") {
            $('.star1').attr('src','img/30star.png');
        }
        if(appreciation1Img == "Satisfait") {
            $('.star1').attr('src','img/40star.png');
        }
        if(appreciation1Img == "Très satisfait") {
            $('.star1').attr('src','img/50star.png');
        }
        
        if(appreciation2Img == "Très insatisfait") {
            $('.star2').attr('src','img/10star.png');
        }
        if(appreciation2Img == "Insatisfait") {
            $('.star2').attr('src','img/20star.png');
        }
        if(appreciation2Img == "Moyen") {
            $('.star2').attr('src','img/30star.png');
        }
        if(appreciation2Img == "Satisfait") {
            $('.star2').attr('src','img/40star.png');
        }
        if(appreciation2Img == "Très satisfait") {
            $('.star2').attr('src','img/50star.png');
        }
        
        if(appreciation3Img == "Très insatisfait") {
            $('.star3').attr('src','img/10star.png');
        }
        if(appreciation3Img == "Insatisfait") {
            $('.star3').attr('src','img/20star.png');
        }
        if(appreciation3Img == "Moyen") {
            $('.star3').attr('src','img/30star.png');
        }
        if(appreciation3Img == "Satisfait") {
            $('.star3').attr('src','img/40star.png');
        }
        if(appreciation3Img == "Très satisfait") {
            $('.star3').attr('src','img/50star.png');
        }
        
        if(appreciation4Img == "Très insatisfait") {
            $('.star4').attr('src','img/10star.png');
        }
        if(appreciation4Img == "Insatisfait") {
            $('.star4').attr('src','img/20star.png');
        }
        if(appreciation4Img == "Moyen") {
            $('.star4').attr('src','img/30star.png');
        }
        if(appreciation4Img == "Satisfait") {
            $('.star4').attr('src','img/40star.png');
        }
        if(appreciation4Img == "Très satisfait") {
            $('.star4').attr('src','img/50star.png');
        }
        
        if(appreciation5Img == "Très insatisfait") {
            $('.star5').attr('src','img/10star.png');
        }
        if(appreciation5Img == "Insatisfait") {
            $('.star5').attr('src','img/20star.png');
        }
        if(appreciation5Img == "Moyen") {
            $('.star5').attr('src','img/30star.png');
        }
        if(appreciation5Img == "Satisfait") {
            $('.star5').attr('src','img/40star.png');
        }
        if(appreciation5Img == "Très satisfait") {
            $('.star5').attr('src','img/50star.png');
        }
        
        if(appreciation6Img == "Très insatisfait") {
            $('.star6').attr('src','img/10star.png');
        }
        if(appreciation6Img == "Insatisfait") {
            $('.star6').attr('src','img/20star.png');
        }
        if(appreciation6Img == "Moyen") {
            $('.star6').attr('src','img/30star.png');
        }
        if(appreciation6Img == "Satisfait") {
            $('.star6').attr('src','img/40star.png');
        }
        if(appreciation6Img == "Très satisfait") {
            $('.star6').attr('src','img/50star.png');
        }
        
        if(appreciation7Img == "Très insatisfait") {
            $('.star7').attr('src','img/10star.png');
        }
        if(appreciation7Img == "Insatisfait") {
            $('.star7').attr('src','img/20star.png');
        }
        if(appreciation7Img == "Moyen") {
            $('.star7').attr('src','img/30star.png');
        }
        if(appreciation7Img == "Satisfait") {
            $('.star7').attr('src','img/40star.png');
        }
        if(appreciation7Img == "Très satisfait") {
            $('.star7').attr('src','img/50star.png');
        }
        $('.fondBlanc2').hide();
        $('.fondBlanc3').show();
        
        $('.pseudo').keyup(function () {
                var value = $(this).val();
                $('.valuePseudo').text(value);
            })
            .keyup();

        $('.titreComm').keyup(function () {
                var valueTitreComm = $(this).val();
                $('.valueTitreComm').text(valueTitreComm);
            })
            .keyup();

        $('#tag').keyup(function () {
                var valueComm = $(this).val();
                $('.valueComm').text(valueComm);
            })
            .keyup();
    })


    /*$('.suivant3').click(function () {
        window.open('/', '_self', false);
    })*/

    $('.modifier').click(function () {
        $('.confirmation').hide();
        $('.fondBlanc2, .fondBlanc3').show();
    })
    
    $('.retour2').click(function(){
        $('.fondBlanc3').hide();
        $('.fondBlanc1').show();
    })

    $('.suivant6').click(function(event){
        if($('.pseudo').val() == '') {
            event.preventDefault();
            $('.manqueLePseudo').text('Vous avez oublié de mettre un pseudo').css({
                color: 'palevioletred',
                marginLeft : '40px'
            })
        } else {
            console.log('ok');
        }
    })
    
});