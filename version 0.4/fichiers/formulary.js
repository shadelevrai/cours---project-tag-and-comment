$(function () {
    $('#texteBienvenue1').hide();
    $('#texteBienvenue1').fadeIn(1000);
    $('#texteBienvenue3').hide();
    $('#texteBienvenue3').delay(1000).fadeIn(1000);
    $('#texteBienvenue2').hide();
    $('#texteBienvenue2').delay(2500).fadeIn(1000);

    $('.suivant').click(function () {
        window.open('/formulary2', '_self', false);
    })

    $('.oui').click(function () {
        window.open('/formulary3', '_self', false);
    })

    $('.suivant2').click(function () {
        window.open('/formulary4', '_self', false);
    })

    $('.suivant3').click(function () {
        window.open('/formulary5', '_self', false);
    })

    $('.suivant4').click(function () {
        window.open('/', '_self', false);
    })


    /*******************************************clique de la page 3***************************/

    $('.divReponse1_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse1_2').css('backgroundColor', 'white');
        $('.divReponse1_3').css('backgroundColor', 'white');
        $('.divReponse1_4').css('backgroundColor', 'white');
        $('.divReponse1_5').css('backgroundColor', 'white');
    })

    $('.divReponse1_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse1_1').css('backgroundColor', 'white');
        $('.divReponse1_3').css('backgroundColor', 'white');
        $('.divReponse1_4').css('backgroundColor', 'white');
        $('.divReponse1_5').css('backgroundColor', 'white');
    })

    $('.divReponse1_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse1_2').css('backgroundColor', 'white');
        $('.divReponse1_1').css('backgroundColor', 'white');
        $('.divReponse1_4').css('backgroundColor', 'white');
        $('.divReponse1_5').css('backgroundColor', 'white');
    })

    $('.divReponse1_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse1_3').css('backgroundColor', 'white');
        $('.divReponse1_5').css('backgroundColor', 'white');
        $('.divReponse1_2').css('backgroundColor', 'white');
        $('.divReponse1_1').css('backgroundColor', 'white');
    })

    $('.divReponse1_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse1_2').css('backgroundColor', 'white');
        $('.divReponse1_3').css('backgroundColor', 'white');
        $('.divReponse1_4').css('backgroundColor', 'white');
        $('.divReponse1_1').css('backgroundColor', 'white');
    })


    $('.divReponse2_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse2_2').css('backgroundColor', 'white');
        $('.divReponse2_3').css('backgroundColor', 'white');
        $('.divReponse2_4').css('backgroundColor', 'white');
        $('.divReponse2_5').css('backgroundColor', 'white');
    })

    $('.divReponse2_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse2_1').css('backgroundColor', 'white');
        $('.divReponse2_3').css('backgroundColor', 'white');
        $('.divReponse2_4').css('backgroundColor', 'white');
        $('.divReponse2_5').css('backgroundColor', 'white');
    })

    $('.divReponse2_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse2_1').css('backgroundColor', 'white');
        $('.divReponse2_2').css('backgroundColor', 'white');
        $('.divReponse2_4').css('backgroundColor', 'white');
        $('.divReponse2_5').css('backgroundColor', 'white');
    })

    $('.divReponse2_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse2_1').css('backgroundColor', 'white');
        $('.divReponse2_2').css('backgroundColor', 'white');
        $('.divReponse2_3').css('backgroundColor', 'white');
        $('.divReponse2_5').css('backgroundColor', 'white');
    })

    $('.divReponse2_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse2_1').css('backgroundColor', 'white');
        $('.divReponse2_2').css('backgroundColor', 'white');
        $('.divReponse2_3').css('backgroundColor', 'white');
        $('.divReponse2_4').css('backgroundColor', 'white');
    })


    $('.divReponse3_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse3_2').css('backgroundColor', 'white');
        $('.divReponse3_3').css('backgroundColor', 'white');
        $('.divReponse3_4').css('backgroundColor', 'white');
        $('.divReponse3_5').css('backgroundColor', 'white');
    })

    $('.divReponse3_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse3_1').css('backgroundColor', 'white');
        $('.divReponse3_3').css('backgroundColor', 'white');
        $('.divReponse3_4').css('backgroundColor', 'white');
        $('.divReponse3_5').css('backgroundColor', 'white');
    })

    $('.divReponse3_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse3_1').css('backgroundColor', 'white');
        $('.divReponse3_2').css('backgroundColor', 'white');
        $('.divReponse3_4').css('backgroundColor', 'white');
        $('.divReponse3_5').css('backgroundColor', 'white');
    })

    $('.divReponse3_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse3_1').css('backgroundColor', 'white');
        $('.divReponse3_2').css('backgroundColor', 'white');
        $('.divReponse3_3').css('backgroundColor', 'white');
        $('.divReponse3_5').css('backgroundColor', 'white');
    })

    $('.divReponse3_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse3_1').css('backgroundColor', 'white');
        $('.divReponse3_2').css('backgroundColor', 'white');
        $('.divReponse3_3').css('backgroundColor', 'white');
        $('.divReponse3_4').css('backgroundColor', 'white');
    })


    $('.divReponse4_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse4_2').css('backgroundColor', 'white');
        $('.divReponse4_3').css('backgroundColor', 'white');
        $('.divReponse4_4').css('backgroundColor', 'white');
        $('.divReponse4_5').css('backgroundColor', 'white');
    })

    $('.divReponse4_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse4_1').css('backgroundColor', 'white');
        $('.divReponse4_3').css('backgroundColor', 'white');
        $('.divReponse4_4').css('backgroundColor', 'white');
        $('.divReponse4_5').css('backgroundColor', 'white');
    })

    $('.divReponse4_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse4_1').css('backgroundColor', 'white');
        $('.divReponse4_2').css('backgroundColor', 'white');
        $('.divReponse4_4').css('backgroundColor', 'white');
        $('.divReponse4_5').css('backgroundColor', 'white');
    })

    $('.divReponse4_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse4_1').css('backgroundColor', 'white');
        $('.divReponse4_2').css('backgroundColor', 'white');
        $('.divReponse4_3').css('backgroundColor', 'white');
        $('.divReponse4_5').css('backgroundColor', 'white');
    })

    $('.divReponse4_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse4_1').css('backgroundColor', 'white');
        $('.divReponse4_2').css('backgroundColor', 'white');
        $('.divReponse4_3').css('backgroundColor', 'white');
        $('.divReponse4_4').css('backgroundColor', 'white');
    })


    $('.divReponse5_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse5_2').css('backgroundColor', 'white');
        $('.divReponse5_3').css('backgroundColor', 'white');
        $('.divReponse5_4').css('backgroundColor', 'white');
        $('.divReponse5_5').css('backgroundColor', 'white');
    })

    $('.divReponse5_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse5_1').css('backgroundColor', 'white');
        $('.divReponse5_3').css('backgroundColor', 'white');
        $('.divReponse5_4').css('backgroundColor', 'white');
        $('.divReponse5_5').css('backgroundColor', 'white');
    })

    $('.divReponse5_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse5_1').css('backgroundColor', 'white');
        $('.divReponse5_2').css('backgroundColor', 'white');
        $('.divReponse5_4').css('backgroundColor', 'white');
        $('.divReponse5_5').css('backgroundColor', 'white');
    })

    $('.divReponse5_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse5_1').css('backgroundColor', 'white');
        $('.divReponse5_2').css('backgroundColor', 'white');
        $('.divReponse5_3').css('backgroundColor', 'white');
        $('.divReponse5_5').css('backgroundColor', 'white');
    })

    $('.divReponse5_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse5_1').css('backgroundColor', 'white');
        $('.divReponse5_2').css('backgroundColor', 'white');
        $('.divReponse5_4').css('backgroundColor', 'white');
        $('.divReponse5_4').css('backgroundColor', 'white');
    })

    $('.divReponse6_1').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse6_2').css('backgroundColor', 'white');
        $('.divReponse6_3').css('backgroundColor', 'white');
        $('.divReponse6_4').css('backgroundColor', 'white');
        $('.divReponse6_5').css('backgroundColor', 'white');
    })

    $('.divReponse6_2').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse6_1').css('backgroundColor', 'white');
        $('.divReponse6_3').css('backgroundColor', 'white');
        $('.divReponse6_4').css('backgroundColor', 'white');
        $('.divReponse6_5').css('backgroundColor', 'white');
    })

    $('.divReponse6_3').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse6_1').css('backgroundColor', 'white');
        $('.divReponse6_2').css('backgroundColor', 'white');
        $('.divReponse6_4').css('backgroundColor', 'white');
        $('.divReponse6_5').css('backgroundColor', 'white');
    })

    $('.divReponse6_4').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse6_1').css('backgroundColor', 'white');
        $('.divReponse6_2').css('backgroundColor', 'white');
        $('.divReponse6_3').css('backgroundColor', 'white');
        $('.divReponse6_5').css('backgroundColor', 'white');
    })

    $('.divReponse6_5').click(function () {
        $(this).css('backgroundColor', 'antiquewhite');
        $('.divReponse6_1').css('backgroundColor', 'white');
        $('.divReponse6_2').css('backgroundColor', 'white');
        $('.divReponse6_3').css('backgroundColor', 'white');
        $('.divReponse6_4').css('backgroundColor', 'white');
    })
})