var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017/jeux';
//var fichierJs = require('./fichiers/indexNonJquery.js');
var myDate = new Date;


app.use(express.static(__dirname + '/fichiers'));
app.use(bodyParser.urlencoded({
    extended: true
}));


//------------------Page enquete-----------------------------//
app.get('/', function (req, res) {
    res.render('index.html');
})

app.get('/index2', function (req, res) {
    res.sendFile('index2.html', {
        root: 'fichiers'
    });
})

app.get('/index3', function (req, res) {
    res.sendFile('index3.html', {
        root: 'fichiers'
    });
})

app.get('/index4', function (req, res) {
    res.sendFile('index4.html', {
        root: 'fichiers'
    });
})


//----------------------Envois d'infos--------------------------//
app.post('/envoiInfo', function (req, res) {
    var comm = req.body.commentaire;
    var avis1 = req.body.radios;
    var avis2 = req.body.radios2;
    var avis3 = req.body.radios3;
    var avis4 = req.body.radios4;
    var avis5 = req.body.radios5;
    var accord = req.body.accord;
    console.log('--------------------');
    console.log('Commentaire : ' + comm);
    console.log('Premier avis : ' + avis1);
    console.log('Second avis : ' + avis2);
    console.log('Troisième avis : ' + avis3);
    console.log('Quatrième avis : ' + avis4);
    console.log('Cinquième avis : ' + avis5);
    console.log('Accord de contacte : ' + accord);
    console.log('date : ' + myDate);
    console.log('--------------------');
    res.redirect('index4');

    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').insert({
                Commentaire: comm,
                Avis01: parseFloat(avis1),
                Avis02: parseFloat(avis2),
                Avis03: parseFloat(avis3),
                Avis04: parseFloat(avis4),
                Avis05: parseFloat(avis5),
                Date: myDate
            });

            db.collection('temp').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    console.log(result);
                }
            });

        }
    });

})


app.post('/pageAdmin', function (req, res) {
    res.redirect('http://localhost:8080/admin.html');
})

//---------------------------------test--------------------------------//



//----------------------------Get et post-------------------------------------//
app.get('/api/affiche', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);

            db.collection('temp').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json(result);
                }
            });
        }
    });
})



var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});


/*

Etape 01 > Front page d'accueil :
    Page d'acceuil
    Page de validation
    Page d'enquete
    Page de remerciement
Etape 02 > Back page d'accueil :
    Envoi des infos dans la BDD
        Schématisé les infos envoyées
Etape 03 > Adapter la page de modération :


*/