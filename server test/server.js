//Le modules
var express = require('express'),
    bodyParser = require("body-parser"),
    session = require('express-session'),
    passport = require('passport'),
    cookieParser = require('cookie-parser'),
    app = express(),
    mongoose = require('mongoose'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    ObjectId = require('mongodb').ObjectID,
    //url = 'mongodb://cernnunos:Zmfgvknrzs11@ds141209.mlab.com:41209/jeux',
    url = 'mongodb://127.0.0.1:27017/jeux',
    device = require('express-device'),
    morgan = require('morgan'),
    fs = require('fs'),
    path = require('path'),
    log4js = require('log4js'),
    configDB = require('./config/database.js'),
    flash = require('connect-flash'),

    //Module complémentaire pour MORGAN
    accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
        flags: 'a'
    }),

    myDate = new Date;


mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

app.use(cookieParser('SECRET'));
app.use(device.capture()); //Module pour connaitre la plate-forme de connection (PC, tablette, mobile...)
app.use(express.static(__dirname + '/app'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(morgan('combined', {
    stream: accessLogStream
}));
app.use(session({
    secret: 's3Cur3',
    name: 'sessionId',
})); // Système de session. A faire.
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.set('view engine', 'ejs');
app.set('view engine', 'jade'); // Template HTML
app.set('views', './app'); // Dire à JADE où sont les fichiers.


log4js.configure({
    appenders: [
        {
            type: 'console'
        },
        {
            type: 'file',
            filename: 'console.log',
            category: 'Console'
        }
  ]
}); // Système de log

var logger = log4js.getLogger('Console');

app.get('/', function(req, res) {
    console.log('ok');
    res.render('index.ejs');
})

var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});